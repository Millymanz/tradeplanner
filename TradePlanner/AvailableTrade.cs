﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner
{
    public class AvailableTrade
    {
        public string Symbol { get; set; }
        public DateTime DiscoveryDateTime { get; set; }
        public DateTime ExpiryDateTime { get; set; }
        public int Rank { get; set; }
    }
}
