﻿using System;
using System.Collections.Generic;
using System.Text;
using Binance.API.Csharp.Client.Utils;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class TimeMgr : ITimeMgr
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public long ConvertDateTimeToTimestamp(DateTime value)
        {
            //Utilities.GenerateTimeStampLong()

            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }

        public long ConvertDateTimeToTimestampAlt(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalMilliseconds - (long)(new TimeSpan(0,0,44, 1000).TotalMilliseconds);
        }

        public DateTime ConvertTimestampToDateTime(long timestamp)
        {
            var fulltimeInMilliseconds = timestamp * 1000;

            var convertedDT = Epoch.AddMilliseconds(fulltimeInMilliseconds);
            var resr = convertedDT.ToUniversalTime();
            return resr;
        }

        public DateTime ConvertTimestampToDateTimeLocal(long timestamp)
        {
            var fulltimeInMilliseconds = timestamp;

            var convertedDT = Epoch.AddMilliseconds(fulltimeInMilliseconds);
            var resr = convertedDT.ToLocalTime();
            return resr;
        }
    }
}
