﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner
{
    //virtual trade object
    public class VirtualTrade
    {
        public Guid ID { get; set; }
        public Guid StrategyID { get; set; }
        public string Symbol { get; set; }
        public DateTime BuyDateTime { get; set; }
        public decimal BuyPrice { get; set; }
        public DateTime SellDateTime { get; set; }
        public decimal SellPrice { get; set; }
        public bool Success { get; set; }

        public VirtualTrade()
        {
            Success = false;
        }
    }
}
