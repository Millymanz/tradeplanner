﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Net;
using System.Security.Cryptography;
using System.Xml.XPath;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;

namespace TradePlanner
{
    public class RestClient
    {
        private IConfigurationRoot _configuration;

        public RestClient(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        public static string ComputeSignature(string stringToSign)
        {
            string secret = "yKb69Lf1ZfzRNEPvoZPihGO047i7I7DPCjRDuTNQQVIkZHnBaoJMSSgGz5smCqRQ";
            using (var hmacsha256 = new HMACSHA256(Convert.FromBase64String(secret)))
            {
                var bytes = Encoding.ASCII.GetBytes(stringToSign);
                var hashedBytes = hmacsha256.ComputeHash(bytes);



                var encodedSignature = Convert.ToBase64String(hashedBytes);
                encodedSignature = System.Web.HttpUtility.UrlEncode(encodedSignature);
                return encodedSignature;
            }
        }

        public static string ComputeSignatureAlt(string stringToSign)
        {
            string secretKey = "yKb69Lf1ZfzRNEPvoZPihGO047i7I7DPCjRDuTNQQVIkZHnBaoJMSSgGz5smCqRQ";

            // Generate a new globally unique identifier for the salt
            var salt = stringToSign;

            // Initialize the keyed hash object using the secret key as the key
            HMACSHA256 hashObject = new HMACSHA256(Encoding.UTF8.GetBytes(secretKey));

            // Computes the signature by hashing the salt with the secret key as the key
            var signature = hashObject.ComputeHash(Encoding.UTF8.GetBytes(salt));

            // Base 64 Encode
            var encodedSignature = Convert.ToBase64String(signature);

            // URLEncode
            encodedSignature = System.Web.HttpUtility.UrlEncode(encodedSignature);

            return encodedSignature;
        }

        public string GetFromRestServiceAlt(string url)
        {


            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            try
            {
                var apiUrl = url;
                WebRequest request = WebRequest.Create(apiUrl);
                // Set the Method property of the request to POST.
                request.Method = "GET";


                //request.Headers.Add("X-Mashape-Key", "rAxpHYY11omshnDRGxOiYNpNgjb8p1d8Zt2jsnrCJiSPuH3NPk");

                request.Headers.Add("X-CMC_PRO_API_KEY", "a7b2ffed-91ec-40f1-81aa-4051c0988f47");
                request.Headers.Add("Accepts", "application/json");




               // request.ContentType = "application/json";
                // Get the response.
                WebResponse response = request.GetResponse();
                // Get the stream containing content returned by the server.
                var dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                Console.WriteLine(responseFromServer);
                // Clean up the streams.
                reader.Close();
                response.Close();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return "";
        }
       
        public string GetFromRestServiceAuth(string url)
        {
            string responseFromServer = "";
            Stream dataStream;
            try
            {
                
                var apiUrl = url;
                WebRequest request = WebRequest.Create(apiUrl);
                // Set the Method property of the request to POST.
                request.Method = "GET";

                request.Headers.Add("X-MBX-APIKEY", _configuration.GetSection("BinanceAPIKey").Value);

                request.Headers.Add("Accepts", "*/*");
                request.Headers.Add("Connection", "keep-alive");
                request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                request.Headers.Add("Content-Type", "application/json");

                request.ContentType = "application/json";
                // Get the response.

                using (WebResponse response = request.GetResponse())
                {
                    // Get the stream containing content returned by the server.
                    dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader.Close();
                    response.Close();
                    return responseFromServer;
                }
                
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return "";
        }

        public string GetFromRestService(string url, string methodMakingCall)
        {

            try
            {
                var apiUrl = url;
                WebRequest request = WebRequest.Create(apiUrl);
                // Set the Method property of the request to POST.
                request.Method = "GET";

                request.Headers.Add("X-CMC_PRO_API_KEY", "a7b2ffed-91ec-40f1-81aa-4051c0988f47");
                request.Headers.Add("Accepts", "application/json");

                request.ContentType = "application/json";
                // Get the response.

                using (WebResponse response = request.GetResponse())
                {
                    // Get the stream containing content returned by the server.
                    var dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader.Close();
                    response.Close();
                    return responseFromServer;
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(methodMakingCall + " :: " + ex.ToString());
                Logger.Log(methodMakingCall + " :: " + ex.ToString());
            }
            return "";
        }
        
        private void TagRemoval(HtmlDocument document, string tag)
        {
            var finaltag = "//" + tag;
            var head = document.DocumentNode.SelectNodes(finaltag);
            if (head != null)
            {
                foreach (var item in head)
                {
                    item.RemoveAllChildren();
                    item.RemoveAll();
                    item.Remove();
                }
                var desDD = document.DocumentNode.InnerHtml;
            }
        }

        private string RemoveUnwantedTags(string data)
        {
            if (string.IsNullOrEmpty(data)) return string.Empty;

            var document = new HtmlDocument();
            document.LoadHtml(data);            

            TagRemoval(document, "head");
            TagRemoval(document, "form");
            TagRemoval(document, "script");
            TagRemoval(document, "style");
            TagRemoval(document, "link");
            TagRemoval(document, "noscript");
            TagRemoval(document, "source");
            TagRemoval(document, "a");            
  

            //var acceptableTags = new String[] { "strong", "em", "u" };
            var acceptableTags = new String[] { "" };

            var nodes = new Queue<HtmlNode>(document.DocumentNode.SelectNodes("./*|./text()"));
            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                if (!acceptableTags.Contains(node.Name) && node.Name != "#text")
                {
                    var childNodes = node.SelectNodes("./*|./text()");

                    if (childNodes != null)
                    {
                        foreach (var child in childNodes)
                        {
                            nodes.Enqueue(child);
                            parentNode.InsertBefore(child, node);
                        }
                    }
                    parentNode.RemoveChild(node);
                }
            }
            var finalText = document.DocumentNode.InnerHtml.Trim();

            string result = System.Text.RegularExpressions.Regex.Replace(finalText, @"\r\n?|\n\<!--", " ");
            return result;
        }
    }
}
