﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Binance.API.Csharp.Client.Domain.Interfaces;
using Binance.API.Csharp.Client.Models.Account;
using Binance.API.Csharp.Client.Models.Enums;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class StrategyTester
    {
        private IDataModel _dataModel;
        private IEntryEngine _entryEngine;
        private IPatternRecognition _patternRecognition;
        private IDataFeed _dataFeed;
        private IEmailManager _emailManager;
        private IConfigurationRoot _configuration;
        private ITimeMgr _timeMgr;
        public ICurrentDateTime CurrentDateTime { get; set; }

        public bool TestMode { get; set; }
        private ConcurrentDictionary<string, EntryDateTime> _entryList;
        public ConcurrentDictionary<string, EntryDateTime> EntryList
        {
            get { return _entryList;  }
        }
        private Object thisLock = new Object();
        private Object thisLockChecks = new Object();
        private Object availableTradesLock = new Object();


        private NewOrder _currentTrade;

        private IReporter _reporter;

        private List<StrategyDefinition>  _strategyDefinitions;
        private ConcurrentDictionary<StrategyDefinition, List<VirtualTrade>>  _virtualTrades;
        public ConcurrentDictionary<StrategyDefinition, List<VirtualTrade>> VirtualTrades
        {
            get { return _virtualTrades; }
        }

        private DateTime _expiryDateTime;
        public DateTime ExpiryDateTime
        {
            get { return _expiryDateTime; }
        }

        private StrategyDefinition _currentStrategyDefinition;
        public StrategyDefinition CurrentStrategyDefinition
        {
            get { return _currentStrategyDefinition; }
        }

        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public StrategyTester(IDataModel dataModel, IEntryEngine entryEngine, IPatternRecognition patternRecognition, 
            IDataFeed dataFeed, IEmailManager emailManager, IConfigurationRoot configuration, ITimeMgr timeMgr, IReporter reporter, ICurrentDateTime currentDateTime)
        {
            _dataModel = dataModel;
            _entryEngine = entryEngine;
            _patternRecognition = patternRecognition;
            _dataFeed = dataFeed;
            _emailManager = emailManager;
            _configuration = configuration;
            _timeMgr = timeMgr;
            CurrentDateTime = currentDateTime;

            _entryList = new ConcurrentDictionary<string, EntryDateTime>();

            _reporter = reporter;

            _virtualTrades = new ConcurrentDictionary<StrategyDefinition, List<VirtualTrade>>();
            _strategyDefinitions = _dataModel.GetAllStrategies();

            foreach (var item in _strategyDefinitions)
            {
                var vTrades = new List<VirtualTrade>();
                _virtualTrades.AddOrUpdate(item, vTrades, (key, oldValue) => vTrades);
            }

            _expiryDateTime = new DateTime(2050, 1, 1);
            TestMode = false;
        }

        private void InitializeVirtualTrades()
        {
            _virtualTrades = new ConcurrentDictionary<StrategyDefinition, List<VirtualTrade>>();
            _strategyDefinitions = _dataModel.GetAllStrategies();

            foreach (var item in _strategyDefinitions)
            {
                var vTrades = new List<VirtualTrade>();
                _virtualTrades.AddOrUpdate(item, vTrades, (key, oldValue) => vTrades);
            }
        }

        public void StartStrategyTest()
        {
            lock (availableTradesLock)
            {
                var threadStart = new ThreadStart(MonitorStrategyTrades);
                var thread = new Thread(threadStart);
                thread.Start();

                var conditions = _dataModel.CheckMarketConditions(CurrentDateTime.Time());

                if (conditions)
                {
                    //get available trades
                    var availableTrades = _dataModel.GetValidAvailableTrades(CurrentDateTime.Time());

                    var tradeData = new Dictionary<string, DataEntity>();

                    foreach (var item in availableTrades)
                    {
                        if (tradeData.ContainsKey(item.Symbol) == false)
                        {
                            tradeData.Add(item.Symbol, _dataFeed.GetTradeData(item.Symbol));
                        }
                    }

                    var pattern = new PatternRecognition(_configuration, _dataModel);
                    pattern.StrategyTestMode = true;

                    var patternEntityList = pattern.IdentifyTradePatterns(tradeData);

                    _entryEngine.CalculateTradeEntryTimesMulti(_entryList, patternEntityList, _strategyDefinitions, _dataModel);
                }
            }
        }

        private void CheckActiveTrades()
        {
            var temptStrategyDef = new StrategyDefinition();

            lock (thisLockChecks)
            {
                for (int y = 0; y < _strategyDefinitions.Count; y++)
                {
                    if (_virtualTrades != null && _virtualTrades.Any())
                    {
                        for (int m = 0; m < _virtualTrades[_strategyDefinitions[y]].Count; m++)
                        {
                            var vt = _virtualTrades[_strategyDefinitions[y]][m];

                            var currentPrice = _dataFeed.GetLatestPrice(vt.Symbol);
                            currentPrice = TradePlanner.ApiAcceptableDecimal(currentPrice);

                            var currentPercentage = ((currentPrice - vt.BuyPrice) / vt.BuyPrice) * 100.0M;

                            var timeSpan = CurrentDateTime.Time() - vt.BuyDateTime;

                            if (currentPercentage >= _strategyDefinitions[y].Percentage)
                            {
                                if (vt.Success == false)
                                {
                                    //success
                                    vt.SellDateTime = CurrentDateTime.Time();
                                    vt.SellPrice = currentPrice;
                                    vt.Success = true;
                                }
                            }
   
                            if (TestMode == false)
                            {
                                //db update
                                _dataModel.UpdateVirtualTrades(vt.ID, vt.SellDateTime, vt.SellPrice, vt.Success);
                            }
                        }
                    }
                }

                if (_virtualTrades != null)
                {
                    var strategies = _virtualTrades.Where(lo =>
                        lo.Value.Count(bn => bn.Success) == 3
                        || lo.Value.Count(bn => bn.Success) == 2 && lo.Value.Count(bn => bn.Success == false) == 1);

                    //var strategies = _virtualTrades.Where(lo =>
                    //    lo.Value.Count(bn => bn.Success) == 3);

                    if (strategies.Any())
                    {
                        var mainstrategy = strategies.OrderByDescending(hm => hm.Key.Weight).FirstOrDefault();
                        
                        _currentStrategyDefinition = mainstrategy.Key;

                        if (TestMode == false && _currentStrategyDefinition.Useable)
                        {
                            if (Convert.ToBoolean(_configuration.GetSection("BearMarketStrategyOnly").Value))
                            {
                                var selectedBear = strategies.Where(mm => mm.Key.Name.ToLower() == "safety").FirstOrDefault();

                                _dataModel.InsertActiveStrategy(selectedBear.Key.ID, CurrentDateTime.Time());
                            }
                            else
                            {
                                //update db
                                _dataModel.InsertActiveStrategy(_currentStrategyDefinition.ID, CurrentDateTime.Time());
                            }
                        }
                    }
                }

                //we dont have to wait for all the tests to finish
                //higher percent strategies can win the race and selected
                //less percent strategies tend to take longer, and work when there is no other better
                //strategies

                if (CurrentDateTime.Time() > _expiryDateTime)
                {
                    InitializeVirtualTrades();

                    _expiryDateTime = new DateTime(2050, 1, 1);
                }
            }
        }

        private void MonitorStrategyTrades()
        {
            CheckActiveTrades();

            lock (thisLock)
            {
                System.Diagnostics.Debug.WriteLine(CurrentDateTime.Time().ToString("yyyy-MM-dd HH:mm"));

                bool lastBuyDone = false;

                var entries = _entryList.Where(mn => mn.Value.TradeEntryDateTime >= CurrentDateTime.Time()
                                                     && mn.Value.TradeEntryDateTime < CurrentDateTime.Time().AddMinutes(15));

                if (entries.Any())
                {

                    //check the current ranks  1 - 6
                    var latestRank = new Dictionary<string, int>();
                    foreach (var em in entries)
                    {
                        if (latestRank.ContainsKey(em.Value.Symbol) == false)
                        {
                            latestRank.Add(em.Value.Symbol, _dataModel.GetLatestRank(em.Value.Symbol));
                        }
                    }

                    var entriesList = new List<EntryDateTime>();
                    foreach (var enIt in entries)
                    {
                        if (entriesList.Any(ui => ui.Symbol == enIt.Value.Symbol) == false)
                        {
                            entriesList.Add(enIt.Value);
                        }
                    }

                    //has coin risen too much since or has the opportunity gone? 5% since entry planning
                    //
                    var validEntries = _entryEngine.IsEntryStillValid(entriesList);

                    if (latestRank.Any() && validEntries.Any())
                    {
                        var selectedKey = latestRank.Where(ui => validEntries.Any(nj => nj == ui.Key))
                            .OrderBy(p => p.Value).FirstOrDefault();

                        foreach (var trendingCoin in latestRank)
                        {
                            if (_entryList.Any(ko => ko.Key.Contains(trendingCoin.Key)))
                            {
                                var desiredOrderPrice = _dataFeed.GetLatestPrice(trendingCoin.Key);
                                desiredOrderPrice = TradePlanner.ApiAcceptableDecimal(desiredOrderPrice);

                                if (_virtualTrades.Values.Any(mo => mo.Any(pp => pp.Symbol == trendingCoin.Key)) == false)
                                {
                                    if (desiredOrderPrice > 0)
                                    {
                                        var buyPriceDateTime = CurrentDateTime.Time();
                                        for (int y = 0; y < _strategyDefinitions.Count; y++)
                                        {

                                            //3 is the test sample
                                            if (_virtualTrades[_strategyDefinitions[y]].Count < 3)
                                            {
                                                var virtualTrade = new VirtualTrade();
                                              
                                                virtualTrade.Symbol = trendingCoin.Key;
                                                virtualTrade.BuyDateTime = buyPriceDateTime;
                                                virtualTrade.BuyPrice = desiredOrderPrice;
                                                virtualTrade.StrategyID = _strategyDefinitions[y].ID;

                                                _virtualTrades[_strategyDefinitions[y]].Add(virtualTrade);


                                                if (_virtualTrades[_strategyDefinitions[y]].Count == 3)
                                                {
                                                    lastBuyDone = true;
                                                }

                                                if (TestMode == false)
                                                {
                                                    //db insert
                                                    _dataModel.InsertVirtualTrade(virtualTrade.StrategyID,
                                                        virtualTrade.Symbol,
                                                        virtualTrade.BuyDateTime, virtualTrade.BuyPrice);

                                                    virtualTrade.ID = _dataModel.GetVirtualTradeID(virtualTrade.StrategyID,
                                                        virtualTrade.Symbol,
                                                        virtualTrade.BuyDateTime);

                                                    Console.WriteLine(y + ". Virtual Trade Created :: " +
                                                                      virtualTrade.Symbol + " " +
                                                                      virtualTrade.StrategyID);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //remove expired entries and entered positions
            if (_entryList.Any())
            {
                var expired = _entryList.Where(op => op.Value.TradeEntryDateTime < CurrentDateTime.Time()).ToList();

                for (int hn = 0; hn < expired.Count(); hn++)
                {
                    _entryList.TryRemove(expired[hn].Key, out _);
                }
            }

            if (_virtualTrades.All(nm => nm.Value.Count == 3))
            {
                //set expiry for the virtual trades, once this expires we start do welcome new coins and start the strategy test again
                //this can be for a new day or period

                _expiryDateTime = CurrentDateTime.Time().AddHours(_strategyDefinitions.FirstOrDefault().Hours);
            }
        }
    }
}
