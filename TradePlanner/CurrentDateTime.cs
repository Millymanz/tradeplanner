﻿using System;
using System.Collections.Generic;
using System.Text;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class CurrentDateTime : ICurrentDateTime
    {
        public DateTime Time()
        {
            return DateTime.Now;
        }
    }
}