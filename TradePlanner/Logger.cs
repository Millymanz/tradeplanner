﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace TradePlanner
{
    public static class Logger
    {
        private static DataModel _dataModel;
        static Logger()
        {
            var environment = Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "Production";
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            _dataModel = new DataModel(configuration);

        }

        public static void Log(string logMsg)
        {
            if (TradePlanner.TestMode == false)
            {
                _dataModel.InsertEventLogs(logMsg);
            }
        }
    }
}
