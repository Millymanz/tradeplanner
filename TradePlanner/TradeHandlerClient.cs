﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Binance.API.Csharp.Client;
using Binance.API.Csharp.Client.Models.Account;
using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Market;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class TradeHandlerClient : ITradeHandlerClient
    {
        private static ApiClient apiClient;


        private BinanceClient _binanceClient;

        public TradeHandlerClient()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            apiClient = new ApiClient(configuration.GetSection("BinanceAPIKey").Value,
                configuration.GetSection("BinanceSecret").Value);

            _binanceClient = new BinanceClient(apiClient, false);
        }

        public Order GetOrder(string symbol, int orderId, ICurrentDateTime currentDateTime)
        {
            var order = new Order();
            try
            {
                order = _binanceClient.GetOrder(symbol, orderId).Result;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                return null;
            }
            return order;
        }

        public NewOrder PostNewOrder(string symbol, decimal origQty, decimal price, OrderSide orderSide, ICurrentDateTime currentDateTime, int iteration)
        {
            var result = new NewOrder();
            try
            {
                result = _binanceClient.PostNewOrder(symbol, origQty, price, orderSide).Result;
            }
            catch (Exception ex)
            {
                if (iteration == 0)
                {
                    Logger.Log(ex.Message);
                    Logger.Log("Parameters :: " + symbol + " Qty:" + origQty + " Price:" + price + " OrderSide:" +
                               orderSide);
                }
                return null;
            }

            return result;
        }

        public NewOrder PostNewOrderTest(string symbol, decimal origQty, decimal price, OrderSide orderSide, ICurrentDateTime currentDateTime)
        {
            var result = new NewOrder();
            try
            {
                result = _binanceClient.PostNewOrderTest(symbol, origQty, price, orderSide).Result;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
                Logger.Log("Parameters :: " + symbol + " Qty:" + origQty + " Price:" + price + " OrderSide:" + orderSide);
                return null;
            }

            return result;
        }

        public CanceledOrder CancelOrder(string symbol, int orderId, ICurrentDateTime currentDateTime)
        {
            var cancel = new CanceledOrder();

            try
            {
                cancel = _binanceClient.CancelOrder(symbol, orderId).Result;
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return cancel;
        }

        public IEnumerable<Order> GetAllOrders(string symbol, ICurrentDateTime currentDateTime)
        {
            var list = new List<Order>();
            try
            {
                list = _binanceClient.GetAllOrders(symbol).Result.ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }
            return list;
        }

        public IEnumerable<Order> GetCurrentOpenOrders(string symbol, ICurrentDateTime currentDateTime)
        {
            var list = new List<Order>();
            try
            {
                list = _binanceClient.GetCurrentOpenOrders(symbol).Result.ToList();
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return list;
        }

        public AccountInfo GetAccountInfo(ICurrentDateTime currentDateTime)
        {
            var accountinfo = new AccountInfo();
            try
            {
                accountinfo = _binanceClient.GetAccountInfo().Result;

            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message);
            }

            return accountinfo;
        }
    }
}
