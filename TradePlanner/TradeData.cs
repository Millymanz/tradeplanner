﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner
{
    public class TradeData
    {
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public double Volume { get; set; }
        public DateTime DateTime { get; set; }
        public string DateTimeJSON { get; set; }
    }


    public struct DataEntity
    {
        public List<TradeData> Trades { get; set; }
        public string JSON { get; set; }
    }

    public class CoinInfo
    {
        public decimal PriceFilter_MinPrice { get; set; }
        public decimal LotSizeFilter_MinLotSize { get; set; }

        public CoinInfo()
        {
            PriceFilter_MinPrice = -1.0M;
            LotSizeFilter_MinLotSize = -1.0M;
        }
    }
}
