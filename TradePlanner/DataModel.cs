﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class DataModel : IDataModel
    {
        private IConfigurationRoot _configuration;

        public DataModel(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        public static string RoundToNearestFiveMin(DateTime dateItem)
        {
            var reminder = dateItem.Minute % 5;
            var minutes = dateItem.Minute - reminder;

            var tempDatetime = new DateTime(dateItem.Year, dateItem.Month, dateItem.Day, dateItem.Hour, minutes, 0);

            return tempDatetime.ToString("yyyy-MM-dd HH:mm");
        }

        public static DateTime RoundToNearestFiveMinDateVersion(DateTime dateItem)
        {
            var reminder = dateItem.Minute % 5;
            var minutes = dateItem.Minute - reminder;

            return new DateTime(dateItem.Year, dateItem.Month, dateItem.Day, dateItem.Hour, minutes, 0);
        }

        public bool NotTrading(string symbol)
        {
            bool nottradingFlag = false;

            string storedProc = "proc_CheckNotTradingList";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);


                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        rdr["Symbol"].ToString();

                        nottradingFlag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return nottradingFlag;
        }

        public void InsertNotTradingList(string symbol)
        {
            string storedProc = "proc_InsertNotTrading";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }

        public KeyValuePair<string, KeyValuePair<int, DateTime>> GetSoldTrade()
        {
            var kvp = new KeyValuePair<string, KeyValuePair<int, DateTime>>();

            string storedProc = "proc_GetSoldTrades";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var tempDatetime = new DateTime();

                        DateTime.TryParse(rdr["DateTime"].ToString(), out tempDatetime);

                        var orderId = Convert.ToInt32(rdr["OrderId"].ToString());

                        kvp = new KeyValuePair<string, KeyValuePair<int, DateTime>>(rdr["Symbol"].ToString(), 
                            new KeyValuePair<int, DateTime>(orderId, tempDatetime));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return kvp;
        }


        public KeyValuePair<string, DateTime> GetLastestTrade()
        {
            var kvp = new KeyValuePair<string, DateTime>();

            string storedProc = "proc_GetLatestTrade";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var tempDatetime = new DateTime();

                        DateTime.TryParse(rdr["DateTime"].ToString(), out tempDatetime);

                        kvp = new KeyValuePair<string, DateTime>(rdr["Symbol"].ToString(), tempDatetime);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return kvp;
        }

        public List<StrategyDefinition> GetAllStrategies()
        {
            var list = new List<StrategyDefinition>();

            string storedProc = "proc_GetAllStrategies";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var strategyDefinition = new StrategyDefinition();
                        strategyDefinition.ID = Guid.Parse(rdr["ID"].ToString());
                        strategyDefinition.Name = rdr["Name"].ToString();
                        strategyDefinition.Candles = Convert.ToInt32(rdr["Candles"].ToString());
                        strategyDefinition.Hours = Convert.ToInt32(rdr["Hours"].ToString());
                        strategyDefinition.Percentage = Convert.ToDecimal(rdr["Percentage"].ToString());
                        strategyDefinition.Weight = Convert.ToInt32(rdr["Weight"].ToString());
                        strategyDefinition.Useable = Convert.ToBoolean(rdr["Useable"].ToString());

                        list.Add(strategyDefinition);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return list;
        }

        public Guid GetVirtualTradeID(Guid strategyID, string symbol, DateTime buyDateTime)
        {
            Guid item = Guid.Empty;

            string storedProc = "proc_GetVirtualTradeID";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = buyDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@StrategyID", strategyID.ToString());
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.Parameters.AddWithValue("@BuyDateTime", dateTimestr);
                  
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        item = Guid.Parse(rdr["ID"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return item;
        }


        public KeyValuePair<StrategyDefinition, DateTime> GetLatestActiveStrategy()
        {
            var strategyDefinition = new StrategyDefinition();
            var dateTime = new DateTime();

            string storedProc = "proc_GetLatestActiveStrategy";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        strategyDefinition.ID = Guid.Parse(rdr["ID"].ToString());
                        strategyDefinition.Name = rdr["Name"].ToString();
                        strategyDefinition.Candles = Convert.ToInt32(rdr["Candles"].ToString());
                        strategyDefinition.Hours = Convert.ToInt32(rdr["Hours"].ToString());
                        strategyDefinition.Percentage = Convert.ToDecimal(rdr["Percentage"].ToString());
                        strategyDefinition.Weight = Convert.ToInt32(rdr["Weight"].ToString());

                        DateTime.TryParse(rdr["DateTime"].ToString(), out dateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return new KeyValuePair<StrategyDefinition, DateTime>(strategyDefinition, dateTime);
        }


        public int GetLatestRank(string symbol)
        {
            int rank = -1;

            string storedProc = "proc_GetLatestRank";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        rank = Convert.ToInt32(rdr["Rank"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return rank;
        }


        public List<AvailableTrade> GetValidAvailableTrades(DateTime dateItem)
        {
            var list = new List<AvailableTrade>();

            string storedProc = "proc_GetValidAvailableTrades";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Period", dateItem.ToString("yyyy-MM-dd HH:mm"));
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var availableTrade = new AvailableTrade();
                        availableTrade.Symbol = rdr["Symbol"].ToString();
                        availableTrade.DiscoveryDateTime = DateTime.Parse(rdr["DateTime"].ToString());
                        availableTrade.ExpiryDateTime = DateTime.Parse(rdr["ExpiryDateTime"].ToString());
                        availableTrade.Rank = Convert.ToInt32(rdr["Rank"].ToString());

                        list.Add(availableTrade);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return list;
        }

        public void InsertEventLogs(string log)
        {
            string storedProc = "proc_InsertLog";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@DateTime", dateTimestr);
                    sqlCommand.Parameters.AddWithValue("@Log", log);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }
        public void UpdateVirtualTrades(Guid ID, DateTime sellDateTime, decimal sellPrice, bool success)
        {
            string storedProc = "proc_UpdateVirtualTrades";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = sellDateTime.ToString("yyyy-MM-dd HH:mm");
                    if (sellDateTime == new DateTime())
                    {
                        sellDateTime = new DateTime(1900, 1, 1, 0, 0, 0);
                    }
                    dateTimestr = sellDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@ID", ID);
                    sqlCommand.Parameters.AddWithValue("@SellDateTime", dateTimestr);
                    sqlCommand.Parameters.AddWithValue("@SellPrice", sellPrice);
                    sqlCommand.Parameters.AddWithValue("@Success", success);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }

        public void InsertActiveStrategy(Guid ID, DateTime currentDateTime)
        {
            bool notInDB = true;
            int counter = 1;

            while (notInDB)
            {
                string storedProc = "proc_InsertActiveStrategy";

                try
                {
                    string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        var dateTimestr = currentDateTime.ToString("yyyy-MM-dd HH:mm");

                        sqlCommand.Parameters.AddWithValue("@StrategyID", ID);
                        sqlCommand.Parameters.AddWithValue("@CurrentDateTime", dateTimestr);

                        sqlCommand.CommandTimeout = 0;
                        sqlCommand.ExecuteNonQuery();
                        notInDB = false;
                    }
                }
                catch (Exception ex)
                {
                    if (counter == 10)
                    {
                        var message = new StringBuilder();
                        message.AppendLine("Failed To Insert New Strategy Change Into DB Strategy ID :- " + ID);
                        message.AppendLine("");
                        message.AppendLine(ex.ToString());
                        message.AppendLine("");

                        new EmailManager(_configuration).Send(null, "Failed To Insert New Strategy In DB", message.ToString());
                    }
                }
                counter++;

            }
        }

        public ConcurrentDictionary<string, EntryDateTime> GetEntryList(ICurrentDateTime currentDateTime)
        {
            var currentEntries = new ConcurrentDictionary<string, EntryDateTime>();

            string storedProc = "proc_GetLatestEntryList";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@NowDateTime", currentDateTime.Time().ToString("yyyy-MM-dd HH:mm"));

                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var entry = new EntryDateTime();
                        entry.Symbol = rdr["Symbol"].ToString();
                        entry.DiscoveryDateTime = DateTime.Parse(rdr["DiscoveryDateTime"].ToString());
                        entry.TradeEntryDateTime = DateTime.Parse(rdr["TradeEntryDateTime"].ToString());
                        entry.DiscoveryPrice = Convert.ToDecimal(rdr["DiscoveryPrice"].ToString());

                        var nameID = entry.Symbol;

                        if (currentEntries.ContainsKey(nameID) == false)
                        {
                            currentEntries.AddOrUpdate(nameID, entry, (key, oldValue) => entry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }


            return currentEntries;
        }

        public void InsertEntryDateTime(string symbol, DateTime discoveryDateTime, DateTime tradeEntryDateTime, decimal discoveryPrice)
        {
            string storedProc = "proc_InsertTradeEntries";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.Parameters.AddWithValue("@DiscoveryDateTime", discoveryDateTime.ToString("yyyy-MM-dd HH:mm"));
                    sqlCommand.Parameters.AddWithValue("@TradeEntryDateTime", tradeEntryDateTime.ToString("yyyy-MM-dd HH:mm"));
                    sqlCommand.Parameters.AddWithValue("@DiscoveryPrice", discoveryPrice);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }

        public void InsertVirtualTrade(Guid strategyID, string symbol, DateTime buyDateTime, decimal buyPrice)
        {
            string storedProc = "proc_InsertVirtualTrades";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = buyDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@StrategyID", strategyID.ToString());
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.Parameters.AddWithValue("@BuyDateTime", dateTimestr);
                    sqlCommand.Parameters.AddWithValue("@BuyPrice", buyPrice);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }

        public void InsertSoldTrade(string symbol, DateTime currentDateTime, int orderId)
        {
            string storedProc = "proc_InsertSoldTrade";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = currentDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@DateTime", dateTimestr);
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.Parameters.AddWithValue("@OrderId", orderId);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }


        public void InsertTrade(string symbol, DateTime currentDateTime)
        {
            string storedProc = "proc_InsertLatestTrade";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = currentDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@DateTime", dateTimestr);
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);

                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }
        }

        public bool CheckMarketConditions(DateTime dateItem)
        {
            var datestr = RoundToNearestFiveMin(dateItem);

            bool goodConditions = false;

            string storedProc = "proc_CheckMarketConditions";

            SqlParameter outputIdParam = new SqlParameter("@ROWSAFFECTED", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.AddWithValue("@Period", datestr);

                    sqlCommand.Parameters.Add(outputIdParam);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();

                    if (Convert.ToInt32(outputIdParam.Value) == 0)
                    {
                        goodConditions = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }


            return goodConditions;
        }
    }
}
