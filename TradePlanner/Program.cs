﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading;


namespace TradePlanner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Trade Planner";
            Console.WriteLine("App Started");


            var scheduleChecker = new ScheduleChecker();

            var threadStart = new ThreadStart(scheduleChecker.StartTrading);
            var thread = new Thread(threadStart);
            thread.Start();

            Logger.Log("Application Start");

            Console.ReadLine();
            Console.ReadLine();
        }
    }
}
