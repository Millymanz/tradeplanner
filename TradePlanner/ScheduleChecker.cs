﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Timers;
using System.Text.RegularExpressions;

// added for access to RegistryKey
using Microsoft.Win32;
// added for access to socket classes
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Binance.API.Csharp.Client;
using Binance.API.Csharp.Client.Models.Account;
using Microsoft.Extensions.Configuration;

namespace TradePlanner
{
    public class ScheduleChecker
    {
        private String _triggerTime;
        System.Timers.Timer actionTimer;

        private TradePlanner _tradePlanner;
        
        private StrategyTester _strategyTester;

        public ScheduleChecker()
        {
            var environment = Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "Production";
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();


            var dataModel = new DataModel(configuration);
            var timeMgr = new TimeMgr();
            var dataFeed = new DataFeed(configuration, timeMgr, dataModel);
            var emailer = new EmailManager(configuration);
            var reporter = new Reporter(emailer);
            var currentDateTime = new CurrentDateTime();

            string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
   
            reporter.GeneralMessage("Application Start Machine :: " + hostName, "Application Start Up ");

            _tradePlanner = new TradePlanner(dataModel, new EntryEngine(dataFeed, currentDateTime, configuration, reporter), new PatternRecognition(configuration, dataModel),
                new DataFeed(configuration, timeMgr, dataModel), emailer, configuration, timeMgr, new TradeHandlerClient(), reporter, true, new CurrentDateTime());

            var stEntryEngine = new EntryEngine(dataFeed, currentDateTime, configuration, reporter);
            stEntryEngine.StrategyTestMode = true;

            _strategyTester = new StrategyTester(dataModel, stEntryEngine, new PatternRecognition(configuration, dataModel),
                 new DataFeed(configuration, timeMgr, dataModel), emailer, configuration, timeMgr, reporter, currentDateTime);
        }

        public void SystemEvents_TimeChanged(object sender, EventArgs e)
        {
            actionTimer.Stop();
            actionTimer.Enabled = false;

            actionTimer.Interval = CalculateInterval();

            actionTimer.Enabled = true;
            actionTimer.Start();
        }

        public void StartTrading()
        {
            SystemEvents.TimeChanged += new EventHandler(SystemEvents_TimeChanged);

            actionTimer = new System.Timers.Timer();

            actionTimer.Elapsed += new ElapsedEventHandler(Task);

            actionTimer.Interval = CalculateInterval();
            actionTimer.AutoReset = true;

            actionTimer.Start();
        }

        private void TradePlannerRun()
        {
            Console.WriteLine("");
            Console.WriteLine("//----START----//");
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            _strategyTester.StartStrategyTest();

            _tradePlanner.StartTradePlan();

            Console.WriteLine("//----END----//");
            Console.WriteLine("");//gap
        }

        private void Task(object sender, ElapsedEventArgs e)
        {
            actionTimer.Stop();
            actionTimer.Enabled = false;

            TradePlannerRun();

            //Tell DBM to start import
            actionTimer.Interval = CalculateInterval();
            actionTimer.Enabled = true;
            actionTimer.Start();
        }

        private double CalculateInterval()
        {
            return 60000;
        }
    }
}
