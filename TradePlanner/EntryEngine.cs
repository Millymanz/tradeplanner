﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public struct EntryDateTime
    {
        public string Symbol;
        public DateTime DiscoveryDateTime;
        public DateTime TradeEntryDateTime;
        public decimal DiscoveryPrice;
    }

    public class EntryEngine : IEntryEngine
    {
        private IDataFeed _dataFeed;
        public bool StrategyTestMode { get; set; }
        public ICurrentDateTime CurrentDateTime { get; set; }

        private IConfigurationRoot _configuration;

        private IReporter _reporter;

        private string _previousInvalid;

        public EntryEngine(IDataFeed dataFeed, ICurrentDateTime currentDateTime, IConfigurationRoot configuration, IReporter reporter)
        {
            _dataFeed = dataFeed;
            CurrentDateTime = currentDateTime;
            _configuration = configuration;
            _reporter = reporter;
            _previousInvalid = "";
        }

        public void CalculateTradeEntryTimes(ConcurrentDictionary<string, EntryDateTime> entryList, List<PatternEntity> patternEntity, StrategyDefinition strategyDefinition, IDataModel dataModel)
        {
            var gracePeriod = Convert.ToDouble(_configuration.GetSection("GracePeriodMinutes").Value);

            //var passed = patternEntity.Where(mo => mo.CurrentZone =="P0" || mo.CurrentZone =="P1");

            var passed = patternEntity;

            foreach (var item in passed)
            {
                if (entryList.ContainsKey(item.Symbol) == false)
                {
                    var entry = new EntryDateTime();
                    entry.Symbol = item.Symbol;
                    entry.DiscoveryDateTime = CurrentDateTime.Time();

                    if (patternEntity.Where(op => op.Symbol == item.Symbol).Any())
                    {
                        var paddingForEntry = strategyDefinition.Candles * 5; //5min timeframe

                        entry.TradeEntryDateTime = patternEntity.Where(op => op.Symbol == item.Symbol).FirstOrDefault()
                            .Stems.FirstOrDefault().SteemDateTime.AddMinutes(paddingForEntry);

                        entry.DiscoveryPrice = _dataFeed.GetLatestPrice(item.Symbol);

                        //xxx candles from stem peak
                    }

                    //only if the current time is less than trade entry time, with grace period
                    if (entry.DiscoveryDateTime <= entry.TradeEntryDateTime.AddMinutes(gracePeriod))
                    {
                        //due to virtual trades, we have to accomodate many entries for the same symbol
                        //for strategy tester
                        var nameID = item.Symbol + "_" + strategyDefinition.Name.ToString();

                        if (entryList.ContainsKey(nameID) == false)
                        {
                            entryList.AddOrUpdate(nameID, entry, (key, oldValue) => entry);

                            if (StrategyTestMode == false)
                            {
                                Logger.Log("Trade Entry Potentials :: " + item.Symbol + " Time Of Entry :: " +
                                           entry.TradeEntryDateTime.ToString("yyyy-MM-dd HH:mm"));


                                dataModel.InsertEntryDateTime(item.Symbol, entry.DiscoveryDateTime,
                                    entry.TradeEntryDateTime, entry.DiscoveryPrice);
                            }
                        }
                    }
                }
            }
        }

        public void CalculateTradeEntryTimesMulti(ConcurrentDictionary<string, EntryDateTime> entryList, List<PatternEntity> patternEntity, List<StrategyDefinition> strategyDefinition, IDataModel dataModel)
        {
            foreach (var sd in strategyDefinition)
            {
                CalculateTradeEntryTimes(entryList, patternEntity, sd, dataModel);
            }
        }


        public List<string> IsEntryStillValid(List<EntryDateTime> entryList)
        {
            //using real time data feed, get current price and see if coin has already risen by 5% or more.
            //only valid ones get added to list

            var valid = new List<string>();

            foreach (var en in entryList)
            {
                var currentPrice = _dataFeed.GetLatestPrice(en.Symbol);
                currentPrice = TradePlanner.ApiAcceptableDecimal(currentPrice);

                var percentGrowth = ((currentPrice - en.DiscoveryPrice) / en.DiscoveryPrice) * 100;

                if (percentGrowth <= 5.0M)
                {
                    valid.Add(en.Symbol);
                }
                else
                {
                    if (StrategyTestMode == false && _previousInvalid != en.Symbol)
                    {
                        var percent = Math.Round(percentGrowth, 3);

                        var msg = en.Symbol + " has exceed the entry level " + percent +
                                  " too much growth for entry";
                        var title = en.Symbol + " Entry No Longer Valid";
                        _reporter.GeneralMessage(msg, title);

                        _previousInvalid = en.Symbol;
                    }
                }
            }

            return valid;
        }
    }
}
