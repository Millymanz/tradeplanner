﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Binance.API.Csharp.Client.Models.Account;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class Reporter : IReporter
    {
        private IEmailManager _emailMgr;
        private string _testOnly;

        public Reporter(IEmailManager emailMgr)
        {
            _emailMgr = emailMgr;

            if (TradePlanner.TestMode)
            {
                _testOnly = "TEST :- ";
            }
        }

        public void GeneralMessage(string msg, string title)
        {
            if (TradePlanner.TestMode == false)
            {
                var message = new StringBuilder();

                message.AppendLine("\n\n ");
                message.AppendLine(msg);

                _emailMgr.Send(null, _testOnly + title + DateTime.Now.ToString(TradePlanner.DateFormat), message.ToString());
            }
        }

        public void BuyOrderExecuted(NewOrder newOrder, decimal price, double quantity, int rank, ConcurrentDictionary<string, EntryDateTime> entryList)
        {
            if (TradePlanner.TestMode == false)
            {
                var message = new StringBuilder();

                message.AppendLine("\n\n ");
                message.AppendLine("<b>" + newOrder.Symbol + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>Buy Price :: " + price + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("Quantity :: " + quantity);
                message.AppendLine("<br>");
                message.AppendLine("Rank :: " + rank);
                message.AppendLine("<br><br>");
                message.AppendLine("\n");


                foreach (var pr in entryList)
                {
                    message.AppendLine("\n\n ");
                    message.AppendLine("<b>" + pr.Value.Symbol + "</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("Discovery Time :: " + pr.Value.DiscoveryDateTime.ToString(TradePlanner.DateFormat));
                    message.AppendLine("<br>");
                    message.AppendLine(
                        "Trade Entry Time :: " + pr.Value.TradeEntryDateTime.ToString(TradePlanner.DateFormat));
                    message.AppendLine("<br><br>");
                    message.AppendLine("\n");
                }

                message.AppendLine("Date :" + DateTime.Now.ToString(TradePlanner.DateFormat));
                message.AppendLine("Buy order sent\n");

                _emailMgr.Send(null, _testOnly + "BUY Order SENT " + DateTime.Now.ToString(TradePlanner.DateFormat), message.ToString());
            }
        }

        public void SellOrderExecuted(NewOrder newOrder, string strategyName, decimal percentageSale, double portfolioSize, decimal sellPrice)
        {
            if (TradePlanner.TestMode == false)
            {
                var message = new StringBuilder();

                message.AppendLine("\n\n ");
                message.AppendLine("<b>" + newOrder.Symbol + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>Strategy Name :: " + strategyName + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("Percentage :: " + Math.Round(percentageSale, 4) + "%");
                message.AppendLine("<br>");
                message.AppendLine("Portfolio Size :: " + portfolioSize + "%");
                message.AppendLine("<br>");
                message.AppendLine("<b>Sell Price :: " + sellPrice + "</b>");
                message.AppendLine("<br><br>");
                message.AppendLine("\n");

                message.AppendLine("Date :" + DateTime.Now.ToString(TradePlanner.DateFormat));
                message.AppendLine("Sell order sent\n");

                _emailMgr.Send(null, _testOnly + "SELL Order SENT - SETUP " + DateTime.Now.ToString(TradePlanner.DateFormat),
                    message.ToString());
            }
        }

        public void SoldExecuted(Order newOrder,  double portfolioSize)
        {
            if (TradePlanner.TestMode == false)
            {
                var message = new StringBuilder();

                message.AppendLine("\n\n ");
                message.AppendLine("<b>" + newOrder.Symbol + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>" + newOrder.OrderId + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>" + newOrder.Price + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>" + newOrder.OrigQty + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("Portfolio Size :: " + portfolioSize);
                message.AppendLine("<br><br>");
                message.AppendLine("\n");

                message.AppendLine("Date :" + DateTime.Now.ToString(TradePlanner.DateFormat));
                message.AppendLine("Sold\n");

                _emailMgr.Send(null, _testOnly + newOrder.Symbol + " :: SOLD " + DateTime.Now.ToString(TradePlanner.DateFormat),
                    message.ToString());
            }
        }

        public void ReportStatus(NewOrder currentTrade, KeyValuePair<StrategyDefinition, DateTime> newstrategyChange, double hoursWithinTrade, ConcurrentDictionary<string, EntryDateTime> entryList)
        {
            if (TradePlanner.TestMode == false)
            {
                var message = new StringBuilder();

                if (currentTrade != null)
                {
                    message.AppendLine("\n\n ");
                    message.AppendLine("<b>Symbol :: " + currentTrade.Symbol + "</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>OrderId :: " + currentTrade.OrderId + "</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>ClientOrderId :: " + currentTrade.ClientOrderId + "</b>");
                    message.AppendLine(" ");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>Strategy Name :: " + newstrategyChange.Key.Name + "</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>Hours :: " + newstrategyChange.Key.Hours + "</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>Percentage :: " + Math.Round(newstrategyChange.Key.Percentage, 3) + "%</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>Candles :: " + newstrategyChange.Key.Candles + "</b>");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>Hours In Trade :: " + hoursWithinTrade + "</b>");
                    message.AppendLine("<br><br>");
                    message.AppendLine("\n");

                    if (entryList.Any())
                    {
                        message.AppendLine("<b>Potential Trade Entries</b>");

                        foreach (var entry in entryList)
                        {
                            message.AppendLine("\n\n ");
                            message.AppendLine("<b>Symbol :: " + entry.Value.Symbol + " ->> Entry : " +
                                               entry.Value.TradeEntryDateTime.ToString(TradePlanner.DateFormat) +
                                               "</b>");
                            message.AppendLine("<br>");
                            message.AppendLine("<b>Discovery Price :: " + entry.Value.DiscoveryPrice + "</b>");
                            message.AppendLine(" ");
                        }
                    }
                }
                else
                {
                    message.AppendLine("\n\n ");
                    message.AppendLine("<b>No Trades Active. System Alive and Functional</b>");
                    message.AppendLine("\n");
                }

                message.AppendLine("Date :" + DateTime.Now.ToString(TradePlanner.DateFormat));

                _emailMgr.Send(null, _testOnly + "Trade Planner Status - Alive " + DateTime.Now.ToString(TradePlanner.DateFormat),
                    message.ToString());
            }
        }

        public void ChangeOfStrategy(KeyValuePair<StrategyDefinition, DateTime> newstrategyChange, KeyValuePair<StrategyDefinition, DateTime> oldstrategy)
        {
            if (TradePlanner.TestMode == false)
            {

                var message = new StringBuilder();

                message.AppendLine("\n\n ");
                message.AppendLine("<b>Strategy Name :: " + newstrategyChange.Key.Name + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>Hours :: " + newstrategyChange.Key.Hours + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>Percentage :: " + Math.Round(newstrategyChange.Key.Percentage, 3) + "</b>");
                message.AppendLine("<br>");
                message.AppendLine("<b>Candles :: " + newstrategyChange.Key.Candles + "</b>");
                message.AppendLine("<br>");
                message.AppendLine(
                    "<b>Discovery :: " + newstrategyChange.Value.ToString(TradePlanner.DateFormat) + "</b>");
                message.AppendLine("<br><br>");
                message.AppendLine("\n");

                if (oldstrategy.Key != null)
                {
                    message.AppendLine("\n\n ");
                    message.AppendLine("Old Strategy Name :: " + oldstrategy.Key.Name + "");
                    message.AppendLine("<br>");
                    message.AppendLine("Old Hours :: " + oldstrategy.Key.Hours + "");
                    message.AppendLine("<br>");
                    message.AppendLine("Old Percentage :: " + Math.Round(oldstrategy.Key.Percentage, 3) + "");
                    message.AppendLine("<br>");
                    message.AppendLine("Old Candles :: " + oldstrategy.Key.Candles + "");
                    message.AppendLine("<br>");
                    message.AppendLine("<b>Discovery :: " + oldstrategy.Value.ToString(TradePlanner.DateFormat) + "</b>");
                    message.AppendLine("<br><br>");
                    message.AppendLine("\n");
                }

                message.AppendLine("Date :" + DateTime.Now.ToString(TradePlanner.DateFormat));
                message.AppendLine("Strategy Switch\n");

                _emailMgr.Send(null, _testOnly + "Strategy Switch " + DateTime.Now.ToString(TradePlanner.DateFormat), message.ToString());
            }
        }
    }
}
