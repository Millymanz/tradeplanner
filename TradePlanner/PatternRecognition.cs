﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class PatternRecognition : IPatternRecognition
    {
        private IConfigurationRoot _configuration;
        private string _basePath;
        public bool DeleteFiles { get; set; }
        public bool StrategyTestMode { get; set; }

        private IDataModel _dataModel;

        public PatternRecognition(IConfigurationRoot configuration, IDataModel dataModel)
        {
            _configuration = configuration;

            _basePath = _configuration.GetSection("BaseOutput").Value +"\\" + DateTime.Now.ToString("MMMM yyyy")
                        + "\\" + DateTime.Now.ToString("dd") + DateTime.Now.ToString("MMMM");

            DeleteFiles = false;

            _dataModel = dataModel;
        }

        private void CalculateSteems(PatternEntity patternEn)
        {
            if (patternEn.TradeDataList.Any())
            {
                var lastTwoHours = patternEn.TradeDataList.OrderByDescending(mp => mp.DateTime).Take(24);

                var highestCandle = lastTwoHours.OrderByDescending(gn => gn.High).FirstOrDefault();
                var st = new Stem();
                st.SteemStringDT = highestCandle.DateTimeJSON;
                st.SteemDateTime = highestCandle.DateTime;
                st.SteemPrice = highestCandle.High;
                patternEn.Stems.Add(st);
            }
        }

        private bool WorkOutZones(List<double> highsFound, List<double> finalZoneLines, int previous, out int previousOutput)
        {
            var current = finalZoneLines.LastOrDefault();

            var indexSt = highsFound.IndexOf(current) + 1;

            bool listCompleted = true;

            for (int u = indexSt; u < highsFound.Count; u++)
            {
                var percentageDiff = ((highsFound[u] - current) / current) * 100;

                //if (percentageDiff < -12)
                if (percentageDiff <= -10)
                {
                    finalZoneLines.Add(highsFound[u]);
                    break;
                }
            }

            if (indexSt == previous)
            {
                listCompleted = false;
            }

            previousOutput = indexSt;

            return listCompleted;
        }

        private void CalculateZones(PatternEntity patternEn)
        {
            var kvpListPeaksTroughs = new List<KeyValuePair<double, DateTime>>();
            var selectedLinesFromBoundaries = new List<double>();

            var highsFound = patternEn.TradeDataList.OrderByDescending(m => m.High).Take(1000).ToList();
            foreach (var high in highsFound)
            {
                kvpListPeaksTroughs.Add(new KeyValuePair<double, DateTime>(high.High, high.DateTime));
            }


            var lowsFound = patternEn.TradeDataList.OrderBy(m => m.Low).Take(1000).ToList();
            foreach (var low in lowsFound)
            {
                kvpListPeaksTroughs.Add(new KeyValuePair<double, DateTime>(low.Low, low.DateTime));
            }

            foreach (var selected in kvpListPeaksTroughs)
            {
                var minBoundary = selected.Key * 0.98;
                var maxBoundary = selected.Key * 1.02;

                if ((selected.Key >= minBoundary && selected.Key <= maxBoundary) 
                    ||
                    (selected.Key >= minBoundary && selected.Key <= maxBoundary))
                {
                    if (selectedLinesFromBoundaries.Contains(selected.Key) == false)
                    {
                        selectedLinesFromBoundaries.Add(selected.Key);
                    }
                }
            }


            var finalZoneLines = new List<double>();
            finalZoneLines.Add(selectedLinesFromBoundaries.FirstOrDefault());


            //finalZoneLines.AddRange(selectedLinesFromBoundaries);
            //finalZoneLines.AddRange(lowsFound);

            bool keepRunning = true;
            int previous = -1;
            while (keepRunning)
            {
                keepRunning = WorkOutZones(selectedLinesFromBoundaries, finalZoneLines, previous, out var previousOutput);
                previous = previousOutput;
            }


            foreach (var zoneItem in finalZoneLines)
            {
                patternEn.ZoneBoxes.Add(zoneItem);
            }


            var orderedCollection = patternEn.ZoneBoxes.OrderBy(mo => mo).ToList();

            for (int op = 0; op < orderedCollection.Count(); op++)
            {

                if (op + 1 < orderedCollection.Count())
                {
                    if (op == 0)
                    {
                        var lb = new LabelledBoxes();
                        lb.PatternName = "P" + op;
                        lb.Lower = orderedCollection[op];
                        lb.Upper = orderedCollection[op + 1];

                        patternEn.LabelledBoxes.Add(lb);
                    }
                    else if (op < orderedCollection.Count())
                    {
                        var lb = new LabelledBoxes();
                        lb.PatternName = "P" + op;
                        lb.Lower = orderedCollection[op];
                        lb.Upper = orderedCollection[op + 1];

                        patternEn.LabelledBoxes.Add(lb);
                    }
                }
            }

            if (patternEn.LabelledBoxes.Any(jo => jo.Lower <= patternEn.TradeDataList.LastOrDefault().Close
                                                  && jo.Upper >= patternEn.TradeDataList.LastOrDefault().Close && jo.PatternName == "P0"))
            {
                patternEn.CurrentZone = "P0";
            }
            else if (patternEn.LabelledBoxes.Any(jo => jo.Lower <= patternEn.TradeDataList.LastOrDefault().Close
                                                       && jo.Upper >= patternEn.TradeDataList.LastOrDefault().Close && jo.PatternName == "P1"))
            {
                patternEn.CurrentZone = "P1";
            }
        }

        public List<PatternEntity> IdentifyTradePatterns(Dictionary<string, DataEntity> tradeData)
        {
            var list = new List<PatternEntity>();

            foreach (var item in tradeData)
            {
                var patternEn = new PatternEntity();
                patternEn.TradeDataList = item.Value.Trades;
                patternEn.Symbol = item.Key;

                //calculate steems
                CalculateSteems(patternEn);

                //calculate zones
                CalculateZones(patternEn);

                //if (StrategyTestMode == false)
                {
                    GenerateChartOutput(item, "1stSTAGE", patternEn);
                }

                list.Add(patternEn);
            }
            return list;
        }

        private void GenerateChartOutput(KeyValuePair<string, DataEntity> kvp, string stage, PatternEntity patternEn)
        {
            string dir = _basePath + "\\charts\\" + kvp.Key + "_" + stage;

            try
            {
                if (Directory.Exists(dir) && DeleteFiles)
                {
                    //Directory.Delete(dir);

                    System.IO.DirectoryInfo di = new DirectoryInfo(dir);

                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dirTemp in di.GetDirectories())
                    {
                        dirTemp.Delete(true);
                    }

                    Directory.Delete(dir);
                }

                if (!Directory.Exists(dir))
                {
                    var ditemp = Directory.CreateDirectory(dir);

                    DirectoryCopy(_configuration.GetSection("TemplateChartFiles").Value, dir, true);

                    var tempDir = dir.Replace("\\", "/");
                    tempDir = dir.Replace(" ", "%%20");

                    string pathscript = dir + "\\run_chart.bat";
                    if (!File.Exists(pathscript))
                    {
                        using (StreamWriter sw = File.CreateText(pathscript))
                        {
                            sw.WriteLine("start chrome file:///" + tempDir + "//index.html");
                            sw.WriteLine("cd " + _configuration.GetSection("ChartProxy").Value);
                            sw.WriteLine("D:");
                            sw.WriteLine("node restproxy");
                        }

                        var indexPath = dir + "\\index.js";

                        if (patternEn.ZoneBoxes.Any())
                        {
                            using (StreamWriter sw = File.AppendText(indexPath))
                            {
                                int op = 0;
                                foreach (var zone in patternEn.ZoneBoxes)
                                {
                                    var variable = "zonelineVar" + op;
                                    sw.WriteLine("var " + variable + " = { price: " + zone +
                                                 ", color: 'lightblue', lineWidth: 2, " +
                                                 "lineStyle: LightweightCharts.LineStyle.Solid, axisLabelVisible: true, title: 'zone', } ");

                                    sw.WriteLine(" candleSeries.createPriceLine(" + variable + "); ");
                                    op++;
                                }
                            }
                        }

                        if (patternEn.Stems.Any())
                        {
                            using (StreamWriter sw = File.AppendText(indexPath))
                            {
                                int op = 0;

                                sw.WriteLine("var markers = [];");

                                foreach (var steem in patternEn.Stems)
                                {
                                    var timeGen = (Convert.ToInt64(steem.SteemStringDT) + 3300000) / 1000; //summertime

                                    sw.WriteLine("markers.push({ time: " + timeGen + ", position: 'aboveBar', " +
                                                 "color: '#f68410', shape: 'circle', text: 'Stem "+patternEn.CurrentZone+"' } );");
                                    op++;
                                }
                                sw.WriteLine(" candleSeries.setMarkers(markers); ");
                            }
                        }

                        var description = "";
                        description += "Symbol :: " + patternEn.Symbol;
                        var rank = "Rank :: " + _dataModel.GetLatestRank(patternEn.Symbol);
                        var patternzone = "Current Pattern Zone :: " + patternEn.CurrentZone;
                        var peakprice = "Stem Peak Price:: " + patternEn.Stems.FirstOrDefault().SteemPrice;
                        var peakdatetime = "Stem Peak DateTime:: " + patternEn.Stems.FirstOrDefault().SteemDateTime.ToString("u");
                       

                        var descriptionSecond = "";
                        foreach (var label in patternEn.LabelledBoxes)
                        {
                            descriptionSecond += "<br>Zone :: " + label.PatternName;
                            descriptionSecond += "<br>Upper :: " + label.Upper;
                            descriptionSecond += "<br>Lower :: " + label.Lower;
                        }


                        using (StreamWriter sw = File.AppendText(indexPath))
                        {
                            var descr = description;
                            sw.WriteLine("var info = document.getElementById('desc'); info.innerText = '" + descr + "'; ");
                            sw.WriteLine("var info = document.getElementById('rank'); info.innerText = '" + rank + "'; ");
                            sw.WriteLine("var info = document.getElementById('patternzone'); info.innerText = '" + patternzone + "'; ");
                            sw.WriteLine("var info = document.getElementById('peakprice'); info.innerText = '" + peakprice + "'; ");
                            sw.WriteLine("var info = document.getElementById('peakdatetime'); info.innerText = '" + peakdatetime + "'; ");

                            sw.WriteLine("var info = document.getElementById('descsecond'); info.innerText = '" + descriptionSecond + "'; ");
                            sw.WriteLine("var titleInfo = document.getElementById('symbolTitle'); titleInfo.innerText = '" + patternEn.Symbol + "'; ");
                        }

                        description += "<br>"+ rank;
                        description += "<br>" + patternzone;
                        description += "<br>" + peakprice;
                        description += "<br>" + peakdatetime;

                        description += descriptionSecond;

                        var descriptionPath = dir + "\\"+ patternEn.Symbol + "_description.txt";
                        if (!File.Exists(descriptionPath))
                        {
                            using (StreamWriter sw = File.CreateText(descriptionPath))
                            {
                                var brokenText = description.Split("<br>");

                                foreach (var lineText in brokenText)
                                {
                                    sw.WriteLine(lineText);
                                }
                            }
                        }
                    }


                    string path = dir + "\\sourcedata.js";

                    if (!File.Exists(path))
                    {
                        // Create a file to write to.
                        using (StreamWriter sw = File.CreateText(path))
                        {
                            sw.WriteLine("var data =");
                            sw.WriteLine(kvp.Value.JSON);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }

        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }
    }
}
