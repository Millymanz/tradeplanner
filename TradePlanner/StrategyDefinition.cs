﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner
{
    public class StrategyDefinition
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public int Candles { get; set; }
        public decimal Percentage { get; set; }
        public int Hours { get; set; }
        public bool Useable { get; set; }
        public int Weight { get; set; }
    }
}
