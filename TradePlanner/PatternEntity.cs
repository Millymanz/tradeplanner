﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner
{
    public class PatternEntity
    {
        public string Symbol { get; set; }
        public string CurrentZone { get; set; }


        public List<TradeData> TradeDataList { get; set; }

        public List<Stem> Stems { get; set; }

        public List<double> ZoneBoxes { get; set; }

        public List<LabelledBoxes> LabelledBoxes { get; set; }



        public PatternEntity()
        {
            TradeDataList = new List<TradeData>();
            Stems = new List<Stem>();
            ZoneBoxes = new List<double>();
            LabelledBoxes = new List<LabelledBoxes>();
        }
    }

    public struct Stem
    {
        public string SteemStringDT { get; set; }
        public DateTime SteemDateTime { get; set; }
        public double SteemPrice { get; set; }
    }

    public struct LabelledBoxes
    {
        public string PatternName { get; set; }
        public double Upper { get; set; }
        public double Lower { get; set; }
    }
}
