//Pseudo code
//Step 1: Define chart properties.
//Step 2: Create the chart with defined properties and bind it to the DOM element.
//Step 3: Add the CandleStick Series.
//Step 4: Set the data and render.


//Code
const log = console.log;

const chartProperties = {
    width: 1500,
    height: 600,
    timeScale: {
        timeVisible: true,
        secondsVisible: false,
    },

    rightPriceScale: {
        borderColor: '#D1D4DC',
    },
    layout: {
        backgroundColor: '#ffffff',
        textColor: '#000',
    },
    grid: {
        horzLines: {
            color: '#F0F3FA',
        },
        vertLines: {
            color: '#F0F3FA',
        },
    },
}

const domElement = document.getElementById('tvchart');
const chart = LightweightCharts.createChart(domElement, chartProperties);
const candleSeries = chart.addCandlestickSeries({
    upColor: 'rgb(38,166,154)',
    downColor: 'rgb(255,82,82)',
    wickUpColor: 'rgb(38,166,154)',
    wickDownColor: 'rgb(255,82,82)',
    borderVisible: false,
});




const cdata = data.map(d => {
	
	var tempTime = (d[0]  + 3300000)/ 1000; 

 return { time: tempTime, open: parseFloat(d[1]), high: parseFloat(d[2]), low: parseFloat(d[3]), close: parseFloat(d[4]) }
});


candleSeries.setData(cdata);



var datesForMarkers = [cdata[data.length - 19], cdata[data.length - 39]];
 var indexOfMinPrice = 0;
for (var i = 1; i < datesForMarkers.length; i++) {
     if (datesForMarkers[i].high < datesForMarkers[indexOfMinPrice].high) {
        indexOfMinPrice = i;
 }
}

datesForMarkers = [cdata[data.length - 27]];


// var markers = [];
// for (var i = 0; i < datesForMarkers.length; i++) {

    // markers.push({ time: datesForMarkers[i].time, position: 'aboveBar', color: '#f68410', shape: 'circle', text: 'Stem P1' }
    // );

// }

// candleSeries.setMarkers(markers);



var lineWidth = 1;
var maxPriceLine = {
    price: 2800,
    color: 'lightblue',
    lineWidth: lineWidth,
    lineStyle: LightweightCharts.LineStyle.Solid,
    axisLabelVisible: true,
    title: 'desired entry',
}
candleSeries.createPriceLine(maxPriceLine);


