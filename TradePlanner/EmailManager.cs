﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class EmailManager : IEmailManager
    {
        private readonly int _port;
        private readonly string _host;
        private readonly string _toDefault;
        private readonly string _from;
        private readonly string _username;
        private readonly string _password;
        private readonly string _apiKey;

        private IConfigurationRoot _configuration;
        
        public EmailManager(IConfigurationRoot configuration)
        {
            _configuration = configuration;

            _host = _configuration.GetSection("Emailer").GetSection("Host").Value;
            Int32.TryParse(_configuration.GetSection("Emailer").GetSection("Host").Value, out _port);
            _username = _configuration.GetSection("Emailer").GetSection("Username").Value;
            _password = _configuration.GetSection("Emailer").GetSection("Password").Value;
            _toDefault = _configuration.GetSection("Emailer").GetSection("EmailTo").Value;
            _from = _configuration.GetSection("Emailer").GetSection("EmailFrom").Value;
            _apiKey = _configuration.GetSection("EmailSendGrid").GetSection("apiKey").Value;
        }

        public void SendOld(string[] recipientList, string emailSubject, string msg)
        {
            var mailMessage = new MailMessage { From = new MailAddress(_from), Subject = emailSubject, Body = msg };

            try
            {
                if (string.IsNullOrEmpty(msg)) { return; }

                var smtpClient = new SmtpClient(_host)
                {
                    EnableSsl = false,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false

                    //EnableSsl = true,
                    //Timeout = 10000,
                    //DeliveryMethod = SmtpDeliveryMethod.Network,
                    //UseDefaultCredentials = false
                };

                if (!string.IsNullOrEmpty(_username) && !string.IsNullOrEmpty(_password)) smtpClient.Credentials = new NetworkCredential(_username, _password);

                var addresses = recipientList;

                if (addresses == null || !addresses.Any())
                {
                    addresses = _toDefault.Split(';');
                }

                foreach (var item in addresses)
                {
                    mailMessage.To.Add(item);
                }

                mailMessage.IsBodyHtml = true; //remove

                mailMessage.BodyEncoding = Encoding.UTF8;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                smtpClient.Send(mailMessage);

                mailMessage.Dispose();
            }
            catch (Exception ex)
            {
                mailMessage.Dispose();

                var msga = "Send :: Email message sending error";
                Console.WriteLine("Email Report Exception: {0}", ex.StackTrace);

                Logger.Log(msga);
            }
        }

        public async Task Send(string[] recipientList, string emailSubject, string msg)
        {
            try
            {
                if (string.IsNullOrEmpty(msg)) { return; }

                var client = new SendGridClient(_apiKey);
                var addresses = _toDefault.Split(';');

                var from = new EmailAddress(_from, "TradePlanner Bot");
                var to = new EmailAddress(addresses.FirstOrDefault(), "Dennis");


                var msgReturn = MailHelper.CreateSingleEmail(from, to, emailSubject, "",msg);
                var response = await client.SendEmailAsync(msgReturn).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Email Report Exception: {0}", ex.StackTrace);

                Logger.Log(ex.ToString());
            }
        }
    }
}
