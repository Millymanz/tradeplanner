﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TradePlanner.Interfaces
{
    public interface IEmailManager
    {
        void SendOld(string[] recipientList, string emailSubject, string msg);

        Task Send(string[] recipientList, string emailSubject, string msg);
    }
}
