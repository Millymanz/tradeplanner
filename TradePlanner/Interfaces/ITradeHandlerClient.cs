﻿using System;
using System.Collections.Generic;
using System.Text;
using Binance.API.Csharp.Client.Models.Account;
using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Market;

namespace TradePlanner.Interfaces
{
    public interface ITradeHandlerClient
    {
        Order GetOrder(string symbol, int orderId, ICurrentDateTime currentDateTime);

        NewOrder PostNewOrder(string symbol, decimal origQty, decimal price, OrderSide orderSide, ICurrentDateTime currentDateTime, int iteration); 
        CanceledOrder CancelOrder(string symbol, int orderId, ICurrentDateTime currentDateTime);

        IEnumerable<Order> GetAllOrders(string symbol, ICurrentDateTime currentDateTime);

        IEnumerable<Order> GetCurrentOpenOrders(string symbol, ICurrentDateTime currentDateTime);

        AccountInfo GetAccountInfo(ICurrentDateTime currentDateTime);

        NewOrder PostNewOrderTest(string symbol, decimal origQty, decimal price, OrderSide orderSide,
            ICurrentDateTime currentDateTime);

    }
}
