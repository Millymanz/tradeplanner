﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner.Interfaces
{
    public interface IEntryEngine
    {
        void CalculateTradeEntryTimes(ConcurrentDictionary<string, EntryDateTime> entryList,
            List<PatternEntity> patternEntity, StrategyDefinition strategyDefinition,
            IDataModel dataModel);

        List<string> IsEntryStillValid(List<EntryDateTime> entryList);

        void CalculateTradeEntryTimesMulti(ConcurrentDictionary<string, EntryDateTime> entryList,
            List<PatternEntity> patternEntity, List<StrategyDefinition> strategyDefinition, IDataModel dataModel);

    }
}
