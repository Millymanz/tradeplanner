﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner.Interfaces
{
    public interface ICurrentDateTime
    {
        public DateTime Time();

    }
}
