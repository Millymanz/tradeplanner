﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner.Interfaces
{
    public interface IDataModel
    {
        public bool CheckMarketConditions(DateTime dateItem);

        public List<AvailableTrade> GetValidAvailableTrades(DateTime dateItem);

        public int GetLatestRank(string symbol);

        public KeyValuePair<string, KeyValuePair<int, DateTime>> GetSoldTrade();

        public KeyValuePair<string, DateTime> GetLastestTrade();

        public void InsertTrade(string symbol, DateTime currentDateTime);

        public void InsertVirtualTrade(Guid strategyID, string symbol, DateTime buyDateTime, decimal buyPrice);

        public void InsertEntryDateTime(string symbol, DateTime discoveryDateTime, DateTime tradeEntryDateTime, decimal discoveryPrice);

        public ConcurrentDictionary<string, EntryDateTime> GetEntryList(ICurrentDateTime currentDateTime);

        public void InsertSoldTrade(string symbol, DateTime currentDateTime, int orderId);

        public void InsertEventLogs(string log);

        public List<StrategyDefinition> GetAllStrategies();

        public void InsertActiveStrategy(Guid ID, DateTime currentDateTime);

        public void UpdateVirtualTrades(Guid ID, DateTime sellDateTime, decimal sellPrice,
            bool success);

        public bool NotTrading(string symbol);

        public void InsertNotTradingList(string symbol);

        public KeyValuePair<StrategyDefinition, DateTime> GetLatestActiveStrategy();

        public Guid GetVirtualTradeID(Guid strategyID, string symbol, DateTime buyDateTime);


    }
}
