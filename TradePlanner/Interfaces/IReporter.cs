﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Binance.API.Csharp.Client.Models.Account;

namespace TradePlanner.Interfaces
{
    public interface IReporter
    {
        void BuyOrderExecuted(NewOrder newOrder, decimal price, double quantity, int rank,
            ConcurrentDictionary<string, EntryDateTime> entryList);

        void SellOrderExecuted(NewOrder newOrder, string strategyName, decimal percentageSale, double portfolioSize,
            decimal sellPrice);

        void ChangeOfStrategy(KeyValuePair<StrategyDefinition, DateTime> newstrategyChange,
            KeyValuePair<StrategyDefinition, DateTime> oldstrategy);

        void ReportStatus(NewOrder currentTrade, KeyValuePair<StrategyDefinition, DateTime> newstrategyChange,
            double hoursWithinTrade, ConcurrentDictionary<string, EntryDateTime> entryList);

        void SoldExecuted(Order newOrder, double portfolioSize);

        void GeneralMessage(string msg, string title);
    }
}
