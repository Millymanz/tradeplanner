﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner.Interfaces
{
    public interface IPatternRecognition
    {
        List<PatternEntity> IdentifyTradePatterns(Dictionary<string, DataEntity> tradeData);

        //void GenerateChartOutput(Dictionary<string, DataEntity> tradeData, string stage, PatternEntity patternEn);
    }
}
