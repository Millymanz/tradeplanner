﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner.Interfaces
{
    public interface IDataFeed
    {
        DataEntity GetTradeData(string symbol);
        DataEntity GetTradeDataFile(string jsonStr);
        decimal GetLatestPrice(string symbol);
        bool QueryOrder(string symbol);
        CoinInfo GetCoinInfo(string symbol);
    }
}
