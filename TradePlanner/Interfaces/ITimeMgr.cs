﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TradePlanner.Interfaces
{
    public interface ITimeMgr
    {
        DateTime ConvertTimestampToDateTime(long timestamp);
        long ConvertDateTimeToTimestamp(DateTime value);
        long ConvertDateTimeToTimestampAlt(DateTime value);

        DateTime ConvertTimestampToDateTimeLocal(long timestamp);
    }
}
