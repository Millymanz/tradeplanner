﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Binance.API.Csharp.Client.Domain.Interfaces;
using Binance.API.Csharp.Client.Models.Account;
using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Utils;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class TradePlanner
    {
        private IDataModel _dataModel;
        private IEntryEngine _entryEngine;
        private IPatternRecognition _patternRecognition;
        private IDataFeed _dataFeed;
        private IEmailManager _emailManager;
        private IConfigurationRoot _configuration;
        private ITimeMgr _timeMgr;
        private ITradeHandlerClient _tradeHandlerClient;

        private Object thisLock = new Object();
        private Object thisLockChecks = new Object();


        public ITradeHandlerClient TradeHandlerClient
        {
            get { return _tradeHandlerClient; }
        }

        public ICurrentDateTime CurrentDateTime { get; set; }

        public static bool TestMode;
        public static string DateFormat = "dd/MM/yyyy HH:mm";

        private ConcurrentDictionary<string, EntryDateTime> _entryList;
        public ConcurrentDictionary<string, EntryDateTime> EntryList
        {
            get { return _entryList; }
        }

        private NewOrder _currentTrade;
        private NewOrder _lastSellOrder;


        private IReporter _reporter;

        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private KeyValuePair<StrategyDefinition, DateTime> _currentStrategyInUse;

        public KeyValuePair<StrategyDefinition, DateTime> CurrentStrategyInUse
        {
            get { return _currentStrategyInUse; }
        }

        private KeyValuePair<StrategyDefinition, DateTime> _previousStrategyInUse;

        private bool NewlyRebooted { get; set; }

        private DateTime _tradeEntryFilled;

        public bool RecoveryMode { get; set; }

        public TradePlanner(IDataModel dataModel, IEntryEngine entryEngine, IPatternRecognition patternRecognition,
            IDataFeed dataFeed, IEmailManager emailManager, IConfigurationRoot configuration, ITimeMgr timeMgr, ITradeHandlerClient tradeClient, IReporter reporter, bool newlyRebooted, ICurrentDateTime currentDateTime)
        {
            _dataModel = dataModel;
            _entryEngine = entryEngine;
            _patternRecognition = patternRecognition;
            _dataFeed = dataFeed;
            _emailManager = emailManager;
            _configuration = configuration;
            _timeMgr = timeMgr;
            _tradeHandlerClient = tradeClient;

            _reporter = reporter;
            CurrentDateTime = currentDateTime;

            NewlyRebooted = newlyRebooted;

            _lastSellOrder = new NewOrder();

            _currentStrategyInUse = new KeyValuePair<StrategyDefinition, DateTime>();
            _previousStrategyInUse = new KeyValuePair<StrategyDefinition, DateTime>();

            TestMode = false;
            RecoveryMode = false;

            _entryList = _dataModel.GetEntryList(CurrentDateTime);
        }

        private void EstablishCorrectStrategy()
        {
            _previousStrategyInUse = _currentStrategyInUse;

            var tempActiveStrategy = _dataModel.GetLatestActiveStrategy();

            if (string.IsNullOrEmpty(tempActiveStrategy.Key.Name) == false)
            {
                _currentStrategyInUse = tempActiveStrategy;
            }

            if (_previousStrategyInUse.Key != null && NewlyRebooted == false)
            {
                if (_currentStrategyInUse.Key.Name != _previousStrategyInUse.Key.Name)
                {
                    _entryList.Clear();

                    _reporter.ChangeOfStrategy(_currentStrategyInUse, _previousStrategyInUse);
                }
            }

            //this is currently not working as expected
            //newlyrebooted flag should only be turned false and offed once, a new
            //strategy with an up to date strategy has been created, at the moment this is 
            //not the case. Also offline scenarios arent going to be many.
            if (NewlyRebooted)
            {
                var timeSpan = CurrentDateTime.Time() - _currentStrategyInUse.Value;

                var expiryHours = Convert.ToInt32(_configuration.GetSection("StrategyFlag")
                    .GetSection("ExpiryInHours_OfflineScenarios").Value);

                if (timeSpan.TotalHours > expiryHours)
                {
                    //Empty, to signify that new strategy test needs to be carried out
                    _currentStrategyInUse = new KeyValuePair<StrategyDefinition, DateTime>();

                    Console.WriteLine("Saved Strategy No Longer Valid : New Strategy Test Underway");
                }
            }
        }

        public void SystemAlive()
        {
            if ((CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 0, 0, 0) &&
                CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 0, 1, 0)
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 12, 0, 0) &&
                 CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 12, 1, 0)))
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 6, 0, 0) &&
                 CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 6, 1, 0))
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 18, 0, 0) &&
                CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 18, 1, 0))
            
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 3, 0, 0) &&
                CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 3, 1, 0))
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 15, 0, 0) &&
                 CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 15, 1, 0))
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 9, 0, 0) &&
                 CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 9, 1, 0))
                ||
                (CurrentDateTime.Time() >= new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 21, 0, 0) &&
                 CurrentDateTime.Time() < new DateTime(CurrentDateTime.Time().Year, CurrentDateTime.Time().Month, CurrentDateTime.Time().Day, 21, 1, 0))

                )
            {
                var timeSpan = CurrentDateTime.Time() - _tradeEntryFilled;
                _reporter.ReportStatus(_currentTrade, _currentStrategyInUse, Math.Round(timeSpan.TotalHours, 2), _entryList);
            }
        }

        public void StartTradePlan()
        {
            Console.WriteLine("ThreadID " + Thread.CurrentThread.ManagedThreadId + " :: OPEN " + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            SystemAlive();

            var threadStart = new ThreadStart(MonitorTrades);
            var thread = new Thread(threadStart);
            thread.Start();

            EstablishCorrectStrategy();

            var conditions = _dataModel.CheckMarketConditions(CurrentDateTime.Time());

            if (conditions && _currentTrade == null && _currentStrategyInUse.Key != null)
            {
                //get available trades
                var availableTrades = _dataModel.GetValidAvailableTrades(CurrentDateTime.Time());

                var tradeData = new Dictionary<string, DataEntity>();

                foreach (var item in availableTrades)
                {
                    if (tradeData.ContainsKey(item.Symbol) == false)
                    {
                        tradeData.Add(item.Symbol, _dataFeed.GetTradeData(item.Symbol));
                    }
                }

                //check for patterns and market them

                var pattern = new PatternRecognition(_configuration, _dataModel);

                var patternEntityList = pattern.IdentifyTradePatterns(tradeData);

                //plan trade enteries
                _entryEngine.CalculateTradeEntryTimes(_entryList, patternEntityList, _currentStrategyInUse.Key, _dataModel);

                //Output
                string outputEntries = "";
                int counter = 0;

                Console.WriteLine("Trade Candidates");
                foreach (var entryListValue in _entryList.Values)
                {
                    outputEntries = counter + ". " + entryListValue.Symbol + " " + entryListValue.DiscoveryDateTime.ToString();
                    outputEntries += " > Entry Time :: " + entryListValue.TradeEntryDateTime.ToString();

                    Console.WriteLine(outputEntries);
                    counter++;
                }

                Console.WriteLine("Strategy :: " + _currentStrategyInUse.Key.Name + " Percent Grab :: " + Math.Round(_currentStrategyInUse.Key.Percentage, 2));
                Console.WriteLine("Candles From Peak :: " + _currentStrategyInUse.Key.Candles);
               
                if (_entryList.Values.Any() == false)
                {
                    Console.WriteLine("0 Waiting For Candidates");
                }

                RecoveryMode = false;
            }

            NewlyRebooted = false;
            
            Console.WriteLine("ThreadID " + Thread.CurrentThread.ManagedThreadId + " :: CLOSE " + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
        }

        private void ExitTradeBadConditions()
        {
            if (IsSellSetupEstablished() && RecoveryMode == false)
            {
                if (_tradeEntryFilled != new DateTime())
                {
                    var timeSpanGone = CurrentDateTime.Time() - _tradeEntryFilled;

                    var hoursUntilRecoveryMode =
                        Convert.ToDouble(_configuration.GetSection("HoursUntilRecoveryMode").Value);

                    if (timeSpanGone.TotalHours >= hoursUntilRecoveryMode && _currentTrade != null &&
                        _lastSellOrder != null)
                    {
                        var cancel =
                            _tradeHandlerClient.CancelOrder(_lastSellOrder.Symbol, _lastSellOrder.OrderId,
                                CurrentDateTime);

                        if (cancel != null)
                        {
                            if (string.IsNullOrEmpty(cancel.ClientOrderId) == false)
                            {
                                var order = _tradeHandlerClient.GetOrder(_currentTrade.Symbol, _currentTrade.OrderId,
                                    CurrentDateTime);

                                if (order != null)
                                {
                                    var recoveryPercentSell =
                                        Convert.ToDecimal(Convert.ToDouble(_configuration.GetSection("RecoveryPercentSell").Value));

                                    var percentSell = recoveryPercentSell;

                                    var currentPrice = _dataFeed.GetLatestPrice(_currentTrade.Symbol);

                                    //sell at whatever price above otherwise sell at recovery price
                                    var orderPriceConvert = order.Price;
                                    if (orderPriceConvert < currentPrice)
                                    {
                                        percentSell = ((currentPrice - orderPriceConvert) / orderPriceConvert) * 100;
                                    }

                                    if (SellSetupCore(percentSell, order))
                                    {
                                        RecoveryMode = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        private Order GetLastLimitSellOrder(string symbol, DateTime date, IEnumerable<Order> allOrders)
        {
            if (string.IsNullOrEmpty(symbol) == false)
            {
                allOrders = _tradeHandlerClient.GetAllOrders(symbol, CurrentDateTime);

                if (allOrders.Any())
                {
                    //var tradeBuyTime = _timeMgr.ConvertDateTimeToTimestamp(date);
                    var tradeBuyTime = Utilities.GenerateTimeStampLong(date);

                    var potentiallySold = allOrders.Where(uo =>
                            uo.Time > tradeBuyTime && uo.Side == "SELL" && uo.Status == "FILLED")
                        .FirstOrDefault();
                    if (potentiallySold != null)
                    {
                        return potentiallySold;
                    }
                }
            }

            return null;
        }

        private void TradeStillActive()
        {
            lock (thisLockChecks)
            {
                var symbolCurrent = _dataModel.GetLastestTrade();

                if (string.IsNullOrEmpty(symbolCurrent.Key) == false)
                {
                    IEnumerable<Order> allOrders = new List<Order>();

                    var potentiallySold = GetLastLimitSellOrder(symbolCurrent.Key, symbolCurrent.Value, allOrders);

                    if (potentiallySold != null)
                    {
                        //Newly fresh sold coin therefore let us record and reflect this live
                        var soldCheck = _dataModel.GetSoldTrade();

                        if (soldCheck.Key != potentiallySold.Symbol && soldCheck.Value.Key != potentiallySold.OrderId)
                        {
                            //If this has happened then there is no current trade
                            _currentTrade = null;
                            RecoveryMode = false;

                            _dataModel.InsertSoldTrade(potentiallySold.Symbol, CurrentDateTime.Time(),
                                potentiallySold.OrderId);

                            var portSize =
                                Convert.ToDouble(_configuration.GetSection("PercentageOfCashToUse").Value);

                            //send email
                            _reporter.SoldExecuted(potentiallySold, portSize);
                        }
                        else
                        {
                            Console.WriteLine("Unknown - Not Sold Yet ~:- " + potentiallySold.Symbol);
                        }
                    }
                    else
                    {
                        var soldCheck = _dataModel.GetSoldTrade();

                        if (soldCheck.Key == symbolCurrent.Key)
                        {
                            //latest trade and last sold trade match therefore, the coin has been bought abd sold already
                            //therefore no current trade active
                            _currentTrade = null;
                            RecoveryMode = false;

                            Console.WriteLine("No Current Trade. Last Coin Bought and Sold Already");
                        }
                        else
                        {

                            //Currently inside of an ACTIVE TRADE
                            //Get info for that trade, as it has not sold yet
                            //give it some grace period/buffer as when you buy will not be spot on the same time as it is recorded in
                            //the db

                            allOrders = _tradeHandlerClient.GetAllOrders(symbolCurrent.Key, CurrentDateTime);

                            var tradeBuyTimeWithBuffer =
                                Utilities.GenerateTimeStampLong(symbolCurrent.Value.AddMinutes(-15));

                            var activeTradeWhichHasSoldYet = allOrders
                                .Where(uo => uo.Time > tradeBuyTimeWithBuffer && uo.Side == "BUY").FirstOrDefault();

                            if (activeTradeWhichHasSoldYet != null)
                            {
                                _currentTrade = new NewOrder();
                                _currentTrade.Symbol = activeTradeWhichHasSoldYet.Symbol;
                                _currentTrade.ClientOrderId = activeTradeWhichHasSoldYet.ClientOrderId;
                                _currentTrade.OrderId = activeTradeWhichHasSoldYet.OrderId;
                                _currentTrade.TransactTime = activeTradeWhichHasSoldYet.Time;

                                Console.WriteLine("Current Trade Set :: " + _currentTrade.Symbol + " " + _currentTrade.OrderId);
                            }
                        }
                    }
                }
            }
        }

        private void MonitorTrades()
        {
            Console.WriteLine("ThreadID " + Thread.CurrentThread.ManagedThreadId + " :: OPEN " + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));


            lock (thisLock)
            {
                TradeStillActive();

                System.Diagnostics.Debug.WriteLine(CurrentDateTime.Time().ToString("yyyy-MM-dd HH:mm"));

                ExitTradeBadConditions();

                //check queue for queued entries or in waiting (wait max 30mins to get desired price of trade is still valid)

                var gracePeriod = Convert.ToDouble(_configuration.GetSection("GracePeriodMinutes").Value);

                var entries = _entryList.Where(mn => mn.Value.TradeEntryDateTime <= CurrentDateTime.Time()
                                                     && mn.Value.TradeEntryDateTime.AddMinutes(gracePeriod) >
                                                     CurrentDateTime.Time());

                if (entries.Any())
                {
                    //check the current ranks  1 - 6
                    var latestRank = new Dictionary<string, int>();
                    foreach (var em in entries)
                    {
                        latestRank.Add(em.Value.Symbol, _dataModel.GetLatestRank(em.Value.Symbol));
                    }


                    var entriesList = new List<EntryDateTime>();
                    foreach (var enIt in entries)
                    {
                        entriesList.Add(enIt.Value);
                    }

                    //has coin risen too much since or has the opportunity gone? 5% since entry planning
                    //
                    var validEntries = _entryEngine.IsEntryStillValid(entriesList);
            
                    if (latestRank.Any() && validEntries.Any())
                    {
                        var selectedKey = latestRank.Where(ui => validEntries.Any(nj => nj == ui.Key))
                            .OrderBy(p => p.Value).FirstOrDefault();

                        var coinReadyForOrderPlacement = _entryList.Where(ko => ko.Value.Symbol == selectedKey.Key)
                            .FirstOrDefault();

                        var desiredOrderPrice = _dataFeed.GetLatestPrice(coinReadyForOrderPlacement.Value.Symbol);

                        desiredOrderPrice = ApiAcceptableDecimal(Convert.ToDecimal(desiredOrderPrice));

                        if (desiredOrderPrice > 0)
                        {
                            var quantity = QuantityToTrade(desiredOrderPrice);

                            if (quantity > -1)
                            {
                                //this can be managed by math round no need for apidecimal call
                                quantity = Math.Round(quantity, 2);

                                var marketConditions = _dataModel.CheckMarketConditions(CurrentDateTime.Time());

                                if (marketConditions)
                                {
                                    if (CheckForOpenOrders() == false)
                                    {
                                        try
                                        {
                                            bool orderSentAlready = false;
                                            if (_currentTrade != null)
                                            {
                                                if (_currentTrade.Symbol == coinReadyForOrderPlacement.Value.Symbol)
                                                {
                                                    orderSentAlready = true;
                                                }
                                            }

                                            if (orderSentAlready == false)
                                            {
                                                _currentTrade = _tradeHandlerClient.PostNewOrder(
                                                    coinReadyForOrderPlacement.Value.Symbol, quantity,
                                                    Convert.ToDecimal(desiredOrderPrice), OrderSide.BUY,
                                                    CurrentDateTime, 0);

                                                if (_currentTrade != null)
                                                {
                                                    if (string.IsNullOrEmpty(_currentTrade.Symbol) == false)
                                                    {
                                                        var msg = "Buy Order Created :: " +
                                                                  coinReadyForOrderPlacement.Value.Symbol +
                                                                  " :: " + quantity + " :: ClientId - " +
                                                                  _currentTrade.ClientOrderId
                                                                  + " :: OrderId - " + _currentTrade.OrderId;

                                                        Logger.Log(msg);
                                                        Console.WriteLine(msg);

                                                        //store current trade in db
                                                        _dataModel.InsertTrade(_currentTrade.Symbol,
                                                            CurrentDateTime.Time());

                                                        var rank = latestRank
                                                            .Where(mm => mm.Key == _currentTrade.Symbol)
                                                            .Select(op => op.Value)
                                                            .FirstOrDefault();

                                                        _reporter.BuyOrderExecuted(_currentTrade, desiredOrderPrice,
                                                            Convert.ToDouble(quantity), rank, _entryList);

                                                    }
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Log(ex.ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("None of the entries are valid :: New entries will be be calculated");
                    }
                }

                SetSellPriceForTrade();
            }

            Console.WriteLine("ThreadID " + Thread.CurrentThread.ManagedThreadId + " :: CLOSE " + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

        }

        private bool CheckForOpenOrders()
        {
            foreach (var eitem in _entryList)
            {
                var openOrders = _tradeHandlerClient.GetCurrentOpenOrders(eitem.Value.Symbol, CurrentDateTime);

                if (openOrders.Any())
                {
                    return true;
                }
            }

            if (_currentTrade != null)
            {
                var order = _tradeHandlerClient.GetOrder(_currentTrade.Symbol, _currentTrade.OrderId, CurrentDateTime);

                if (order != null)
                {
                    if (order.Status != null)
                    {
                        if (order.Status.ToLower() == "filled")
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }


        private bool IsSellSetupEstablished()
        {
            bool completed = false;
            if (_currentTrade != null)
            {
                if (string.IsNullOrEmpty(_currentTrade.Symbol) == false)
                {
                    var order = _tradeHandlerClient.GetOrder(_currentTrade.Symbol, _currentTrade.OrderId,
                        CurrentDateTime);

                    if (order != null)
                    {
                        IEnumerable<Order> allOrders = new List<Order>();

                        //has sell limit order already been placed? If so no need to do it again
                        allOrders = _tradeHandlerClient.GetAllOrders(_currentTrade.Symbol, CurrentDateTime);

                        var alreadySetSellLimitSetup = allOrders
                            .Where(uo => uo.Side == "SELL" && uo.Status == "NEW" && uo.Symbol == _currentTrade.Symbol)
                            .FirstOrDefault();

                        //ensure not all calls come into here only when orders present
                        if (order.Status != null && order.Status.ToLower() == "filled" && alreadySetSellLimitSetup == null)
                        {
                            _tradeEntryFilled = CurrentDateTime.Time();
                            completed = true;
                        }
                        else
                        {
                            if (alreadySetSellLimitSetup != null)
                            {
                                _tradeEntryFilled =
                                    _timeMgr.ConvertTimestampToDateTimeLocal(alreadySetSellLimitSetup.Time);
                                if (_lastSellOrder == null || string.IsNullOrEmpty(_lastSellOrder.Symbol))
                                {
                                    _lastSellOrder = new NewOrder();
                                    _lastSellOrder.Symbol = alreadySetSellLimitSetup.Symbol;
                                    _lastSellOrder.ClientOrderId = alreadySetSellLimitSetup.ClientOrderId;
                                    _lastSellOrder.OrderId = alreadySetSellLimitSetup.OrderId;
                                    _lastSellOrder.TransactTime = alreadySetSellLimitSetup.Time;
                                }
                                completed = true;
                            }
                        }
                    }
                }
            }
            return completed;
        }

        private void SetSellPriceForTrade()
        {
            if (_currentTrade != null)
            {
                if (string.IsNullOrEmpty(_currentTrade.Symbol) == false)
                {
                    var order = _tradeHandlerClient.GetOrder(_currentTrade.Symbol, _currentTrade.OrderId, CurrentDateTime);

                    if (order != null)
                    {
                        if (order.Status != null && _currentStrategyInUse.Key != null)
                        {
                            IEnumerable<Order> allOrders = new List<Order>();

                            //has sell limit order already been placed? If so no need to do it again

                            allOrders = _tradeHandlerClient.GetAllOrders(_currentTrade.Symbol, CurrentDateTime);

                            var alreadySetSellLimitSetup = allOrders
                                .Where(uo => uo.Side == "SELL" && uo.Status == "NEW" && uo.Symbol == _currentTrade.Symbol)
                                .FirstOrDefault();

                            //ensure not all calls come into here only when orders present

                            if (order.Status.ToLower() == "filled" && alreadySetSellLimitSetup == null)
                            {
                                _tradeEntryFilled = CurrentDateTime.Time();

                                var percent = _currentStrategyInUse.Key.Percentage + 0.02M;

                                SellSetupCore(percent, order);
                            }
                        }
                    }
                }
            }
        }

        private bool SellSetupCore(decimal percent, Order order)
        {

            var portSize =
                Convert.ToDouble(_configuration.GetSection("PercentageOfCashToUse").Value);

            bool runSell = false;

            var coinInfo = _dataFeed.GetCoinInfo(order.Symbol);

            if (coinInfo.PriceFilter_MinPrice > -1.0M)
            {
                var percentSell = ((percent) / 100) + 1;
                var desiredOrderSellPrice = order.Price * Convert.ToDecimal(percentSell);

                desiredOrderSellPrice =
                    ApiAcceptableDecimalDetailed(desiredOrderSellPrice, coinInfo.PriceFilter_MinPrice);

                bool sellOrderSettingContinue = true;
                decimal decrementDecimal = 0.0M;

                bool escape = false;

                while (sellOrderSettingContinue)
                {

                    for (int iter = 0; iter < 5; iter++) //we try many times just in case
                    {
                        try
                        {
                            //estimated amount they take
                            var subQty = (coinInfo.LotSizeFilter_MinLotSize * (50.0M + decrementDecimal));
                            var qtyTakenAsFees = order.OrigQty - subQty;

                            var qtyTakenAsFees_Decimal =
                                ApiAcceptableDecimalDetailed(Convert.ToDecimal(qtyTakenAsFees),
                                    coinInfo.LotSizeFilter_MinLotSize);


                            _lastSellOrder = _tradeHandlerClient
                                .PostNewOrder(order.Symbol, qtyTakenAsFees_Decimal, desiredOrderSellPrice,
                                    OrderSide.SELL,
                                    CurrentDateTime, iter);


                            if (_lastSellOrder != null)
                            {
                                sellOrderSettingContinue = false;
                                runSell = true;

                                var msg = "Sell Order Setup Placed % ::" + percent + " :: " + _lastSellOrder +
                                          " portsize :: " + portSize;

                                if (percent < _currentStrategyInUse.Key.Percentage)
                                {
                                    var recoveryPercentSell =
                                        Convert.ToDecimal(
                                            Convert.ToDouble(_configuration.GetSection("RecoveryPercentSell")
                                                .Value));

                                    _reporter.SellOrderExecuted(_lastSellOrder, "RECOVERY MODE",
                                        recoveryPercentSell,
                                        portSize, desiredOrderSellPrice);

                                    msg = "RECOVERY Sell Order Setup Placed % ::" + recoveryPercentSell;
                                }
                                else
                                {
                                    _reporter.SellOrderExecuted(_lastSellOrder, _currentStrategyInUse.Key.Name,
                                        percent,
                                        portSize, desiredOrderSellPrice);
                                }

                                Logger.Log(msg);
                                Console.WriteLine(msg);

                                _entryList.TryRemove(order.Symbol, out _);

                                //Only after we have been able to set sell setup
                                //remove expired entries and entered positions
                                if (_entryList.Any() && _currentTrade != null)
                                {
                                    if (string.IsNullOrEmpty(_currentTrade.Symbol) == false)
                                    {
                                        var expired = _entryList
                                            .Where(op => op.Value.TradeEntryDateTime < CurrentDateTime.Time())
                                            .ToList();

                                        for (int hn = 0; hn < expired.Count(); hn++)
                                        {
                                            _entryList.TryRemove(expired[hn].Key, out _);

                                            Logger.Log("Entry List Clearance :: " + expired[hn].Key);
                                        }
                                    }
                                }
                            }

                            if (qtyTakenAsFees < 0)
                            {
                                escape = true;
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Log(ex.ToString());
                        }
                    }

                    if (escape)
                    {
                        sellOrderSettingContinue = false;
                        //on hold we dont want continuous emails
                        //_reporter.GeneralMessage("Failed to set sell order, qty is now in negative territory. " +
                        //                         "Set Manually.", "Failed Sell Setup");
                    }
                    decrementDecimal = decrementDecimal + 10.0M;
                }
            }
            else
            {
                Console.WriteLine("CoinInfo Error");
            }
            return runSell;
        }

        public static decimal ApiAcceptableDecimalDetailed(decimal givenValue, decimal currentPrice)
        {
            var res = Math.Truncate(givenValue);
            if (res.ToString().Length >= 2)
            {
                givenValue = Math.Round(givenValue, 2);
            }
            else
            {
                var lastportionTemp = givenValue.ToString().Split('.');
                var lastportion = lastportionTemp.LastOrDefault();

                var currentPriceTemp = Normalize(currentPrice).ToString().Split('.');
                var currentportion = currentPriceTemp.LastOrDefault();


                if (lastportion.Length >= 4 && res == 0)
                {
                    //these are low price coins keep as is no need to
                    //round

                    var resRound = Math.Round(givenValue, currentportion.Length);
                    return resRound;
                }
                else
                {
                    givenValue = Math.Round(givenValue, 3);
                }
            }

            return givenValue;
        }

        public static decimal Normalize(decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }


        public static decimal ApiAcceptableDecimal(decimal givenValue)
        {
            var res = Math.Truncate(givenValue);
            if (res.ToString().Length >= 2)
            {
                givenValue = Math.Round(givenValue, 2);
            }
            else
            {
                var lastportionTemp = givenValue.ToString().Split('.');

                var lastportion = lastportionTemp.LastOrDefault();

                if (lastportion.Length >= 4 && res == 0)
                {
                    //these are low price coins keep as is no need to
                    //round
                    return givenValue;
                }
                else
                {
                    givenValue = Math.Round(givenValue, 3);
                }
            }

            return givenValue;
        }

        private decimal QuantityToTrade(decimal price)
        {
            var accountInfo = _tradeHandlerClient.GetAccountInfo(CurrentDateTime);

            if (accountInfo != null)
            {
                var freeMoney = accountInfo.Balances.Where(mp => mp.Asset == "USDT")
                    .FirstOrDefault().Free;

                var amount = freeMoney;
                var tradableAmount =
                    (Convert.ToDecimal(_configuration.GetSection("PercentageOfCashToUse").Value) / 100.00M) * amount;

                var quantity = tradableAmount / price;

                return Convert.ToDecimal(quantity);
            }

            return -1;
        }
    }
}
