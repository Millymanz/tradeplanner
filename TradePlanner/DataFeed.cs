﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner
{
    public class DataFeed : IDataFeed
    {
        private RestClient _restClient;

        private IConfigurationRoot _configuration;

        private ITimeMgr _timeMgr;

        private IDataModel _dataModel;


        public DataFeed(IConfigurationRoot configuration, ITimeMgr timeMgr, IDataModel dataModel)
        {
            _restClient = new RestClient(configuration);
            _configuration = configuration;
            _timeMgr = timeMgr; 
            _dataModel = dataModel;
        }

        public decimal GetLatestPrice(string symbol)
        {
            double price = -0.0;
            decimal tempDecimal = 0.0M;

            var res = _restClient.GetFromRestService(_configuration.GetSection("BinanceDataFeedTicker").Value + symbol, "GetLatestPrice");

            if (string.IsNullOrEmpty(res) == false)
            {
                dynamic json = JsonConvert.DeserializeObject(res);
                if (json != null)
                {
                    tempDecimal = Convert.ToDecimal(json.price);
                }
            }

            return tempDecimal;
        }

        public bool QueryOrder(string symbol)
        {
            var orderId = 1355420297;
            var timestamp = _timeMgr.ConvertDateTimeToTimestampAlt(DateTime.Now);


            var urlPart = "symbol=" + symbol + "&orderId=" + orderId + "&recvWindow=60000"
                          + "&timestamp=" + timestamp;

            var keeyalt = RestClient.ComputeSignature(urlPart);
            var keey = RestClient.ComputeSignatureAlt(urlPart);

            var url = _configuration.GetSection("BinanceTradeOrder").Value + urlPart + "&signature=" + keeyalt;


            var res = _restClient.GetFromRestServiceAuth(url);


            return false;
        }

        public CoinInfo GetCoinInfo(string symbol)
        {
            var coinInfo = new CoinInfo();

            var res = _restClient.GetFromRestService(_configuration.GetSection("BinanceCoinInfo").Value + symbol, "GetCoinInfo");
            
            if (string.IsNullOrEmpty(res) == false)
            {
                dynamic json = JsonConvert.DeserializeObject(res);

                foreach (var sym in json.symbols)
                {
                    foreach (var filter in sym.filters)
                    {
                        if (filter.filterType.ToString() == "PRICE_FILTER")
                        {
                            coinInfo.PriceFilter_MinPrice = Convert.ToDecimal(filter.minPrice);
                        }

                        if (filter.filterType.ToString() == "LOT_SIZE")
                        {
                            coinInfo.LotSizeFilter_MinLotSize = Convert.ToDecimal(filter.minQty);
                        }
                    }
                }
            }

            return coinInfo;
        }

        public DataEntity GetTradeData(string symbol)
        {
            var daEn = new DataEntity();
            daEn.Trades = new List<TradeData>();

            if (_dataModel.NotTrading(symbol) == false)
            {
                var currentTimeRequest = DateTime.Now;

                var res = _restClient.GetFromRestService(_configuration.GetSection("BinanceDataFeed").Value + symbol +
                                                         "&interval=5m&limit=1000", "GetTradeData");
                var coinInfo = GetCoinInfo(symbol);

                if (string.IsNullOrEmpty(res) && coinInfo.LotSizeFilter_MinLotSize < 0M && coinInfo.LotSizeFilter_MinLotSize < 0M)
                {
                    _dataModel.InsertNotTradingList(symbol);
                }

                daEn.JSON = res;

                dynamic json = JsonConvert.DeserializeObject(daEn.JSON);

                if (json != null)
                {
                    foreach (var tradeItem in json)
                    {
                        var tradeData = new TradeData()
                        {
                            Open = Convert.ToDouble(tradeItem[1]),
                            High = Convert.ToDouble(tradeItem[2]),
                            Low = Convert.ToDouble(tradeItem[3]),
                            Close = Convert.ToDouble(tradeItem[4]),
                            Volume = Convert.ToDouble(tradeItem[5]),
                            DateTimeJSON = tradeItem[0].ToString()
                        };

                        daEn.Trades.Add(tradeData);
                    }

                    daEn.Trades[999].DateTime =
                        DataModel.RoundToNearestFiveMinDateVersion(currentTimeRequest).AddMinutes(-5);

                    for (int i = 998; i >= 0; i--)
                    {
                        daEn.Trades[i].DateTime = daEn.Trades[i + 1].DateTime.AddMinutes(-5);
                    }
                }
            }

            return daEn;
        }


        public DataEntity GetTradeDataFile(string jsonStr)
        {
            var daEn = new DataEntity();
            daEn.JSON = jsonStr;
            daEn.Trades = new List<TradeData>();

            dynamic json = JsonConvert.DeserializeObject(daEn.JSON);

            if (json != null)
            {
                foreach (var tradeItem in json)
                {
                    var tradeData = new TradeData()
                    {
                        Open = Convert.ToDouble(tradeItem[1]),
                        High = Convert.ToDouble(tradeItem[2]),
                        Low = Convert.ToDouble(tradeItem[3]),
                        Close = Convert.ToDouble(tradeItem[4]),
                        Volume = Convert.ToDouble(tradeItem[5]),
                        DateTimeJSON = tradeItem[0].ToString(),
                        DateTime = _timeMgr.ConvertTimestampToDateTimeLocal(Convert.ToInt64(tradeItem[0].ToString()))
                    };

                    daEn.Trades.Add(tradeData);
                }
            }

            return daEn;
        }
    }
}
