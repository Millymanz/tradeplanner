﻿using System;
using System.Collections.Generic;
using System.Text;
using Binance.API.Csharp.Client.Utils;
using Xunit;

namespace TradePlanner.Tests
{
    public class TimeConversionTest
    {
        [Fact]
        public void Can_Convert_To_TimeStamp()
        {
            var timeMgr = new TimeMgr();

            //var timeCon = timeMgr.ConvertDateTimeToTimestamp(new DateTime(2021, 6, 14, 10, 25, 0));
            var timeCon = timeMgr.ConvertDateTimeToTimestamp(new DateTime(2021, 6, 18, 22, 00, 00));

            var dateGen = new DateTime(2021, 7, 1, 13, 35, 00);
            var timeStamp = Utilities.GenerateTimeStampLong(dateGen); //use this to get exact time for data
            
            var addeTime = dateGen.AddMinutes(-5000);


            var dateTime = timeMgr.ConvertTimestampToDateTime(timeCon);

        }
    }
}
