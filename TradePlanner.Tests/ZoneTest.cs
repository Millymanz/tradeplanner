using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;
using Xunit;

namespace TradePlanner.Tests
{
    public class ZoneTest
    {
        private IDataFeed _dataFeed;
        private IConfigurationRoot _configuration;
        private IDataModel _dataModel;
        public ZoneTest()
        {
            var environment = Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "Production";

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            _dataModel = new DataModel(_configuration);

            _dataFeed = new DataFeed(_configuration, new TimeMgr(), _dataModel);
        }

        [Fact (Skip="Run when testing Zone and charts output")]
        public void Can_Calculate_Zones()
        {
            var dataStrList = LoadFileData();

            var tradeData = new Dictionary<string, DataEntity>();

            foreach (var item in dataStrList)
            {
                if (tradeData.ContainsKey(item.Key) == false)
                {
                    tradeData.Add(item.Key, _dataFeed.GetTradeDataFile(item.Value));
                }
            }

            var patternRec = new PatternRecognition(_configuration, _dataModel);
            patternRec.DeleteFiles = true;

            var patternList = patternRec.IdentifyTradePatterns(tradeData);
            //patternRec.IdentifyTradePatterns();
        }

        private List<KeyValuePair<string, string>> LoadFileData()
        {
            var data = new List<KeyValuePair<string, string>>();

            string[] fileEntries = Directory.GetFiles(_configuration.GetSection("TestDataFiles").Value);

            foreach (var filesItem in fileEntries)
            {
                if (File.Exists(filesItem))
                {
                    var name = filesItem.Split("_").FirstOrDefault().Split("\\").LastOrDefault();

                    // Read entire text file content in one string    
                    string text = File.ReadAllText(filesItem);
                    data.Add(new KeyValuePair<string, string>(name, text));
                }
            }

            return data;
        }
    }
}
