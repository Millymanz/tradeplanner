﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner.Tests.Scenarios.Scenario2
{
    public class FakeDataModelTwo : IDataModel
    {
        private List<CryptoRankAdvance> _cryptoRanks;

        private IConfigurationRoot _configuration;

        public DateTime CurrentDateTime { get; set; }

        public int Dataset { get; set; }

        public FakeDataModelTwo(IConfigurationRoot configuration, DateTime currentDateTime)
        {
            _configuration = configuration;
            CurrentDateTime = currentDateTime;


            var cryptoRanks = new List<CryptoRank>();
            var path = _configuration.GetSection("StrategyTester").GetSection("RankingTestFiles1").Value;
            if (File.Exists(path))
            {
                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    cryptoRanks = csv.GetRecords<CryptoRank>().ToList();
                }
            }

            _cryptoRanks = new List<CryptoRankAdvance>();

            foreach (var item in cryptoRanks)
            {
                var cRA = new CryptoRankAdvance();
                cRA.Symbol = item.Symbol;
                cRA.CirculatingSupply = item.CirculatingSupply;
                cRA.DateTime = item.DateTime;
                cRA.ID = item.ID;
                cRA.Logo = item.Logo;
                cRA.MarketCap = item.MarketCap;
                cRA.Percentage1h = item.Percentage1h;
                cRA.Percentage24h = item.Percentage24h;
                cRA.Percentage7d = item.Percentage7d;
                cRA.Price = item.Price;
                cRA.Volume = item.Volume;
                cRA.Rank = item.Rank;

                cRA.ProperDateTime = Convert.ToDateTime(item.DateTime);

                _cryptoRanks.Add(cRA);
            }
        }

        public KeyValuePair<string, KeyValuePair<int, DateTime>> GetSoldTrade()
        {
            var kvp = new KeyValuePair<string, KeyValuePair<int, DateTime>>();


            return kvp;
        }

        public void InsertSoldTrade(string symbol, DateTime currentDateTime, int orderId)
        {

        }

        public void InsertEntryDateTime(string symbol, DateTime discoveryDateTime, DateTime tradeEntryDateTime, decimal discoveryPrice)
        {
            
        }

        public ConcurrentDictionary<string, EntryDateTime> GetEntryList(ICurrentDateTime currentDateTime)
        {
            var currentEntries = new ConcurrentDictionary<string, EntryDateTime>();



            return currentEntries;
        }


        public Guid GetVirtualTradeID(Guid strategyID, string symbol, DateTime buyDateTime)
        {
            Guid item = Guid.Empty;

            string storedProc = "proc_GetVirtualTradeID";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = buyDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@StrategyID", strategyID.ToString());
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.Parameters.AddWithValue("@BuyDateTime", dateTimestr);

                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        item = Guid.Parse(rdr["ID"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return item;
        }

        public bool CheckMarketConditions(DateTime dateItem)
        {
            return true;
        }

        public List<AvailableTrade> GetValidAvailableTrades(DateTime dateItem)
        {
            var list = new List<AvailableTrade>();
            var firstEmailDateTime = new DateTime(2021, 5, 5, 2, 30, 0);
            var secondEmailDateTime = new DateTime(2021, 5, 5, 4, 0, 0);
            var thirdEmailDateTime = new DateTime(2021, 5, 5, 15, 30, 0);


            var fourthEmailDateTime = new DateTime(2021, 5, 10, 1, 30, 00);
            var fiveEmailDateTime = new DateTime(2021, 5, 10, 7, 0, 00);
            var sixthEmailDateTime = new DateTime(2021, 5, 10, 8, 30, 00);

            var firstEmailDateTime_1 = new DateTime(2021, 6, 17, 9, 0, 0);
            var secondEmailDateTime_2 = new DateTime(2021, 6, 17, 9, 30, 0);
            var thirdEmailDateTime_3 = new DateTime(2021, 6, 17, 10, 30, 23);
            var fourthEmailDateTime_4 = new DateTime(2021, 6, 17, 12, 30, 00);




            if (dateItem >= firstEmailDateTime && dateItem <= firstEmailDateTime.AddHours(1))
            {
                var al = new AvailableTrade();
                al.Symbol = "EOSUSDT";
                al.DiscoveryDateTime = firstEmailDateTime;
                al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                al.Rank = 3;
                list.Add(al);
            }

            if (dateItem >= secondEmailDateTime && dateItem <= secondEmailDateTime.AddHours(1))
            {
                var al = new AvailableTrade();
                al.Symbol = "BCHUSDT";
                al.DiscoveryDateTime = secondEmailDateTime;
                al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                al.Rank = 4;
                list.Add(al);
            }

            if (dateItem >= thirdEmailDateTime && dateItem <= thirdEmailDateTime.AddHours(1))
            {
                var al = new AvailableTrade();
                al.Symbol = "SCUSDT";
                al.DiscoveryDateTime = thirdEmailDateTime;
                al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                al.Rank = 6;
                list.Add(al);
            }


            if (Dataset == 2)
            {
                if (dateItem >= fourthEmailDateTime && dateItem <= fourthEmailDateTime.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "LSKUSDT";
                    al.DiscoveryDateTime = fourthEmailDateTime;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 3;
                    list.Add(al);
                }

                if (dateItem >= fiveEmailDateTime && dateItem <= fiveEmailDateTime.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "ONEUSDT";
                    al.DiscoveryDateTime = fiveEmailDateTime;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 5;
                    list.Add(al);
                }

                if (dateItem >= sixthEmailDateTime && dateItem <= sixthEmailDateTime.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "XLMUSDT";
                    al.DiscoveryDateTime = sixthEmailDateTime;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 5;
                    list.Add(al);
                }
            }


            if (Dataset == 3)
            {
                if (dateItem >= firstEmailDateTime_1 && dateItem <= firstEmailDateTime_1.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "TFUELUSDT";
                    al.DiscoveryDateTime = firstEmailDateTime_1;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 6;
                    list.Add(al);
                }

                if (dateItem >= secondEmailDateTime_2 && dateItem <= secondEmailDateTime_2.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "THETAUSDT";
                    al.DiscoveryDateTime = secondEmailDateTime_2;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 6;
                    list.Add(al);
                }

                //if (dateItem >= thirdEmailDateTime_3 && dateItem <= thirdEmailDateTime_3.AddHours(1))
                //{
                //    var al = new AvailableTrade();
                //    al.Symbol = "HNTUSDT";
                //    al.DiscoveryDateTime = thirdEmailDateTime_3;
                //    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                //    al.Rank = 5;
                //    list.Add(al);
                //}

                if (dateItem >= fourthEmailDateTime_4 && dateItem <= fourthEmailDateTime_4.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "FTMUSDT";
                    al.DiscoveryDateTime = fourthEmailDateTime_4;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 5;
                    list.Add(al);
                }
            }


            return list;
        }

        public int GetLatestRank(string symbol)
        {
            var rnd = new Random();
            var ren = rnd.Next(1, 5);
            
            return ren;

            //int rank = -1;

            //var rankingList = _cryptoRanks.Where(mp => mp.Symbol == symbol 
            //                                           && mp.ProperDateTime == CurrentDateTime);
            
            //if (rankingList.FirstOrDefault() != null && rankingList.Any() && rankingList != null)
            //{
            //    var temp  = rankingList.FirstOrDefault();

            //    if (temp != null)
            //    {
            //        rank = temp.Rank;
            //    }
            //}
            //else
            //{
            //    rankingList = _cryptoRanks.Where(mp => mp.Symbol == symbol
            //                                           && mp.ProperDateTime == CurrentDateTime.AddMinutes(-1));
                
            //    if (rankingList.FirstOrDefault() != null && rankingList.Any() && rankingList != null)
            //    {
            //        var temp = rankingList.FirstOrDefault();

            //        if (temp != null)
            //        {
            //            rank = temp.Rank;
            //        }
            //    }
            //}
            
            //return rank;
        }

        public KeyValuePair<string, DateTime> GetLastestTrade()
        {
            return new KeyValuePair<string, DateTime>();
        }

        public void InsertTrade(string symbol, DateTime currentDateTime)
        {

        }

        public void InsertVirtualTrade(Guid strategyID, string symbol, DateTime buyDateTime, decimal buyPrice)
        {

        }

        public void InsertEventLogs(string log)
        {

        }

        public List<StrategyDefinition> GetAllStrategies()
        {
            var list = new List<StrategyDefinition>();

            string storedProc = "proc_GetAllStrategies";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var strategyDefinition = new StrategyDefinition();
                        strategyDefinition.ID = Guid.Parse(rdr["ID"].ToString());
                        strategyDefinition.Name = rdr["Name"].ToString();
                        strategyDefinition.Candles = Convert.ToInt32(rdr["Candles"].ToString());
                        strategyDefinition.Hours = Convert.ToInt32(rdr["Hours"].ToString());
                        strategyDefinition.Percentage = Convert.ToDecimal(rdr["Percentage"].ToString());
                        strategyDefinition.Weight = Convert.ToInt32(rdr["Weight"].ToString());
                        strategyDefinition.Useable = Convert.ToBoolean(rdr["Useable"].ToString());


                        list.Add(strategyDefinition);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return list;
        }


        public void InsertActiveStrategy(Guid ID, DateTime currentDateTime)
        {

        }

        public void UpdateVirtualTrades(Guid ID, DateTime sellDateTime, decimal sellPrice, bool success)
        {

        }

        public bool NotTrading(string symbol)
        {
            return false;
        }

        public void InsertNotTradingList(string symbol)
        {

        }

        public KeyValuePair<StrategyDefinition, DateTime> GetLatestActiveStrategy()
        {
            var strategyDefinition = new StrategyDefinition();
            var dateTime = new DateTime();

            string storedProc = "proc_GetLatestActiveStrategy";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        strategyDefinition.ID = Guid.Parse(rdr["ID"].ToString());
                        strategyDefinition.Name = rdr["Name"].ToString();
                        strategyDefinition.Candles = Convert.ToInt32(rdr["Candles"].ToString());
                        strategyDefinition.Hours = Convert.ToInt32(rdr["Hours"].ToString());
                        strategyDefinition.Percentage = Convert.ToDecimal(rdr["Percentage"].ToString());
                        strategyDefinition.Weight = Convert.ToInt32(rdr["Weight"].ToString());

                        DateTime.TryParse(rdr["DateTime"].ToString(), out dateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return new KeyValuePair<StrategyDefinition, DateTime>(strategyDefinition, dateTime);
        }
    }
}
