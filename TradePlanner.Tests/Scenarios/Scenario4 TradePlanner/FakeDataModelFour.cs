﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;

namespace TradePlanner.Tests.Scenarios.Scenario4
{
    public class FakeDataModelFour : IDataModel
    {
        private IConfigurationRoot _configuration;

        public DateTime CurrentDateTime { get; set; }

        public int Dataset { get; set; }

        private DateTime _scenario_STXUSDT;

        private DateTime _sellScenario1_STXUSDT;


        public FakeDataModelFour(IConfigurationRoot configuration, DateTime currentDateTime)
        {
            _configuration = configuration;
            CurrentDateTime = currentDateTime;

            _scenario_STXUSDT = new DateTime(2021, 6, 24, 16, 0, 0);

            _sellScenario1_STXUSDT = new DateTime(2021, 6, 25, 1, 0, 0);

        }
        public KeyValuePair<string, KeyValuePair<int, DateTime>> GetSoldTrade()
        {
            var sym = "";
            if (CurrentDateTime >= _sellScenario1_STXUSDT)
            {
                sym = "STXUSDT";
            }

            var kvp = new KeyValuePair<string, KeyValuePair<int, DateTime>>(sym, new KeyValuePair<int, DateTime>());


            return kvp;
        }

        public bool CheckMarketConditions(DateTime dateItem)
        {
            return true;
        }

        public void InsertEntryDateTime(string symbol, DateTime discoveryDateTime, DateTime tradeEntryDateTime, decimal discoveryPrice)
        {
           
        }

        public void InsertSoldTrade(string symbol, DateTime currentDateTime, int orderId)
        {

        }

        public ConcurrentDictionary<string, EntryDateTime> GetEntryList(ICurrentDateTime currentDateTime)
        {
            var currentEntries = new ConcurrentDictionary<string, EntryDateTime>();

           

            return currentEntries;
        }


        public Guid GetVirtualTradeID(Guid strategyID, string symbol, DateTime buyDateTime)
        {
            Guid item = Guid.Empty;

            string storedProc = "proc_GetVirtualTradeID";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var dateTimestr = buyDateTime.ToString("yyyy-MM-dd HH:mm");

                    sqlCommand.Parameters.AddWithValue("@StrategyID", strategyID.ToString());
                    sqlCommand.Parameters.AddWithValue("@Symbol", symbol);
                    sqlCommand.Parameters.AddWithValue("@BuyDateTime", dateTimestr);

                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        item = Guid.Parse(rdr["ID"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return item;
        }


        public List<AvailableTrade> GetValidAvailableTrades(DateTime dateItem)
        {
            var list = new List<AvailableTrade>();
            var firstEmailDateTime = new DateTime(2021, 6, 24, 13, 30, 0);
            var secondEmailDateTime = new DateTime(2021, 5, 5, 4, 0, 0);
            var thirdEmailDateTime = new DateTime(2021, 5, 5, 15, 30, 0);


            if (Dataset == 4)
            {
                if (dateItem >= firstEmailDateTime && dateItem <= firstEmailDateTime.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "ADAUSDT";
                    al.DiscoveryDateTime = firstEmailDateTime;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 3;
                    list.Add(al);
                }

                if (dateItem >= secondEmailDateTime && dateItem <= secondEmailDateTime.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "CELOUSDT";
                    al.DiscoveryDateTime = secondEmailDateTime;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 4;
                    list.Add(al);
                }
            }

            if (Dataset == 2)
            {
                if (dateItem >= _scenario_STXUSDT && dateItem <= _scenario_STXUSDT.AddHours(1))
                {
                    var al = new AvailableTrade();
                    al.Symbol = "STXUSDT";
                    al.DiscoveryDateTime = thirdEmailDateTime;
                    al.ExpiryDateTime = al.DiscoveryDateTime.AddHours(1);
                    al.Rank = 3;
                    list.Add(al);
                }
            }

            return list;
        }

        public int GetLatestRank(string symbol)
        {
            var rnd = new Random();
            var ren = rnd.Next(1, 5);
            
            return ren;
        }

        public KeyValuePair<string, DateTime> GetLastestTrade()
        {
            if (Dataset == 2)
            {
                if (CurrentDateTime >= _sellScenario1_STXUSDT &&
                    CurrentDateTime < _sellScenario1_STXUSDT.AddMinutes(30))
                {
                    return new KeyValuePair<string, DateTime>("STXUSDT", _sellScenario1_STXUSDT);
                }

                if (CurrentDateTime == new DateTime(2021, 6, 25, 1, 5, 0))
                {
                    return new KeyValuePair<string, DateTime>("STXUSDT", CurrentDateTime);
                }
            }

            return new KeyValuePair<string, DateTime>();
        }

        public void InsertTrade(string symbol, DateTime currentDateTime)
        {

        }

        public void InsertVirtualTrade(Guid strategyID, string symbol, DateTime buyDateTime, decimal buyPrice)
        {

        }

        public void InsertEventLogs(string log)
        {

        }

        public List<StrategyDefinition> GetAllStrategies()
        {
            var list = new List<StrategyDefinition>();

            string storedProc = "proc_GetAllStrategies";

            try
            {
                string connectionString = _configuration.GetConnectionString("TradePlannerDB");

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand(storedProc, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;

                    SqlDataReader rdr = sqlCommand.ExecuteReader();
                    while (rdr.Read())
                    {
                        var strategyDefinition = new StrategyDefinition();
                        strategyDefinition.ID = Guid.Parse(rdr["ID"].ToString());
                        strategyDefinition.Name = rdr["Name"].ToString();
                        strategyDefinition.Candles = Convert.ToInt32(rdr["Candles"].ToString());
                        strategyDefinition.Hours = Convert.ToInt32(rdr["Hours"].ToString());
                        strategyDefinition.Percentage = Convert.ToDecimal(rdr["Percentage"].ToString());
                        strategyDefinition.Weight = Convert.ToInt32(rdr["Weight"].ToString());
                        strategyDefinition.Useable = Convert.ToBoolean(rdr["Useable"].ToString());


                        list.Add(strategyDefinition);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + " :: " + ex.ToString());
            }

            return list;
        }


        public void InsertActiveStrategy(Guid ID, DateTime currentDateTime)
        {

        }

        public void UpdateVirtualTrades(Guid ID, DateTime sellDateTime, decimal sellPrice, bool success)
        {

        }

        public bool NotTrading(string symbol)
        {
            return false;
        }

        public void InsertNotTradingList(string symbol)
        {

        }

        public KeyValuePair<StrategyDefinition, DateTime> GetLatestActiveStrategy()
        {
            var strategyDefinition = new StrategyDefinition();
            var dateTime = DateTime.Now;

            strategyDefinition.ID = Guid.NewGuid();
            strategyDefinition.Name = "Normal Bull Run";
            strategyDefinition.Candles = 20;
            strategyDefinition.Hours = 8;
            strategyDefinition.Percentage = 5;
            strategyDefinition.Weight = 90;

            return new KeyValuePair<StrategyDefinition, DateTime>(strategyDefinition, dateTime);
        }
    }
}
