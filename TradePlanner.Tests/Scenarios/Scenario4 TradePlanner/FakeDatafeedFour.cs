﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TradePlanner.Interfaces;

namespace TradePlanner.Tests.Scenarios.Scenario4
{
    public class FakeDatafeedFour : IDataFeed
    {
        private IConfigurationRoot _configuration;

        private TimeMgr _timeMgr;
        private Dictionary<string, DataEntity> _tradeData;

        public DateTime CurrentDateTime { get; set; }
        public int Dataset { get; set; }
        public bool LoadOtherDataset { get; set; }


        public FakeDatafeedFour(IConfigurationRoot configuration, DateTime currentDateTime)
        {
            _timeMgr = new TimeMgr();
            _configuration = configuration;
            CurrentDateTime = currentDateTime;

            _tradeData = new Dictionary<string, DataEntity>();
            PopulateDataList();

            LoadOtherDataset = false;
        }

        public CoinInfo GetCoinInfo(string symbol)
        {
            var coinInfo = new CoinInfo();
            coinInfo.PriceFilter_MinPrice = 2.0M;
            coinInfo.LotSizeFilter_MinLotSize = 50;

            return coinInfo;
        }


        public DataEntity GetTradeData(string symbol)
        {
            //use current datetime to filter data accordingly

            var tradeDatasFiltered = _tradeData[symbol].Trades.Where(mk => mk.DateTime <= CurrentDateTime);

            var dt = new DataEntity();
            dt.Trades = tradeDatasFiltered.ToList();

            var nextDateItem = _tradeData[symbol].Trades.Where(kl => kl.DateTime == CurrentDateTime.AddMinutes(5));

            if (nextDateItem.Any())
            {
                var temptStr = _tradeData[symbol].JSON.Split(nextDateItem.FirstOrDefault().DateTimeJSON);
                var newJson = temptStr.FirstOrDefault().Substring(0, temptStr.FirstOrDefault().Length - 2 ) +"]";
                dt.JSON = newJson;
            }

            return dt;
        }

        public DataEntity GetTradeDataFile(string jsonStr)
        {
            var daEn = new DataEntity();
            daEn.JSON = jsonStr;
            daEn.Trades = new List<TradeData>();

            dynamic json = JsonConvert.DeserializeObject(daEn.JSON);

            if (json != null)
            {
                foreach (var tradeItem in json)
                {
                    var tradeData = new TradeData()
                    {
                        Open = Convert.ToDouble(tradeItem[1]),
                        High = Convert.ToDouble(tradeItem[2]),
                        Low = Convert.ToDouble(tradeItem[3]),
                        Close = Convert.ToDouble(tradeItem[4]),
                        Volume = Convert.ToDouble(tradeItem[5]),
                        DateTimeJSON = tradeItem[0].ToString(),
                        DateTime = _timeMgr.ConvertTimestampToDateTimeLocal(Convert.ToInt64(tradeItem[0].ToString()))
                    };

                    daEn.Trades.Add(tradeData);
                }
            }

            return daEn;
        }

        public void PopulateDataList()
        {
            var dataStrList = LoadFileData();

            foreach (var item in dataStrList)
            {
                if (_tradeData.ContainsKey(item.Key) == false)
                {
                    var dataREs = GetTradeDataFile(item.Value);
                    if (item.Key == "ADAUSDT")
                    {
                        var ren = dataREs.Trades.Where(mm => mm.DateTime >
                        new DateTime(2021, 6, 23, 2, 0, 0))
                            .OrderBy(ui => ui.DateTime).ToList();

                        dataREs.Trades = ren;
                    }

                    _tradeData.Add(item.Key, dataREs);
                }
            }
        }

        public void Clear()
        {
            _tradeData = new Dictionary<string, DataEntity>();
            //_tradeData.Clear();
        }


        public decimal GetLatestPrice(string symbol)
        {
            if (_tradeData.ContainsKey(symbol))
            {
                var tradeDatasFiltered = _tradeData[symbol].Trades.Where(mk => mk.DateTime == CurrentDateTime);


                if (tradeDatasFiltered.FirstOrDefault() != null && tradeDatasFiltered.Any() &&
                    tradeDatasFiltered != null)
                {
                    var temp = tradeDatasFiltered.FirstOrDefault();

                    if (temp != null)
                    {
                        return Convert.ToDecimal(temp.Close);
                    }
                }
                else
                {
                    try
                    {
                        tradeDatasFiltered = _tradeData[symbol].Trades.Where(mk =>
                            mk.DateTime == DataModel.RoundToNearestFiveMinDateVersion(CurrentDateTime));
                        if (tradeDatasFiltered.Any())
                        {
                            if (tradeDatasFiltered.First() != null)
                            {
                                var close = tradeDatasFiltered.First().Close;
                                return Convert.ToDecimal(close);
                            }
                            else
                            {
                                return -1;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        return -1;
                    }
                }
            }

            return 0.0M;
        }

        public bool QueryOrder(string symbol)
        {
            return false;
        }

        private List<KeyValuePair<string, string>> LoadFileData()
        {
            var data = new List<KeyValuePair<string, string>>();

            string[] fileEntries = Directory.GetFiles(_configuration.GetSection("TradePlanner").GetSection("TestFiles1").Value);

            //if (LoadOtherDataset)
            //{
            //    fileEntries = Directory.GetFiles(_configuration.GetSection("TradePlanner").GetSection("TestFiles1").Value);
            //}

            foreach (var filesItem in fileEntries)
            {
                if (File.Exists(filesItem))
                {
                    var name = filesItem.Split("_").FirstOrDefault().Split("\\").LastOrDefault();

                    // Read entire text file content in one string    
                    string text = File.ReadAllText(filesItem);
                    data.Add(new KeyValuePair<string, string>(name, text));
                }
            }

            return data;
        }

    }
}
