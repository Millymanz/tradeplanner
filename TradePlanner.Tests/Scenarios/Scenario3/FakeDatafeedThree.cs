﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TradePlanner.Interfaces;

namespace TradePlanner.Tests.Scenarios.Scenario3
{
    public class FakeDatafeedThree : IDataFeed
    {
        private IConfigurationRoot _configuration;

        private TimeMgr _timeMgr;
        private Dictionary<string, DataEntity> _tradeData;

        public DateTime CurrentDateTime { get; set; }

        public FakeDatafeedThree(IConfigurationRoot configuration, DateTime currentDateTime)
        {
            _timeMgr = new TimeMgr();
            _configuration = configuration;
            CurrentDateTime = currentDateTime;

            _tradeData = new Dictionary<string, DataEntity>();
            PopulateDataList();
        }

        public CoinInfo GetCoinInfo(string symbol)
        {
            var coinInfo = new CoinInfo();
            coinInfo.PriceFilter_MinPrice = 2.0M;
            coinInfo.LotSizeFilter_MinLotSize = 50;

            return coinInfo;
        }

        public DataEntity GetTradeData(string symbol)
        {
            //use current datetime to filter data accordingly

            var tradeDatasFiltered = _tradeData[symbol].Trades.Where(mk => mk.DateTime <= CurrentDateTime);

            var dt = new DataEntity();
            dt.Trades = tradeDatasFiltered.ToList();

            var nextDateItem = _tradeData[symbol].Trades.Where(kl => kl.DateTime == CurrentDateTime.AddMinutes(5));

            if (nextDateItem.Any())
            {
                var temptStr = _tradeData[symbol].JSON.Split(nextDateItem.FirstOrDefault().DateTimeJSON);
                var newJson = temptStr.FirstOrDefault().Substring(0, temptStr.FirstOrDefault().Length - 2 ) +"]";
                dt.JSON = newJson;
            }

            return dt;
        }

        public DataEntity GetTradeDataFile(string jsonStr)
        {
            var daEn = new DataEntity();
            daEn.JSON = jsonStr;
            daEn.Trades = new List<TradeData>();

            dynamic json = JsonConvert.DeserializeObject(daEn.JSON);

            if (json != null)
            {
                foreach (var tradeItem in json)
                {
                    var tradeData = new TradeData()
                    {
                        Open = Convert.ToDouble(tradeItem[1]),
                        High = Convert.ToDouble(tradeItem[2]),
                        Low = Convert.ToDouble(tradeItem[3]),
                        Close = Convert.ToDouble(tradeItem[4]),
                        Volume = Convert.ToDouble(tradeItem[5]),
                        DateTimeJSON = tradeItem[0].ToString(),
                        DateTime = _timeMgr.ConvertTimestampToDateTimeLocal(Convert.ToInt64(tradeItem[0].ToString()))
                    };

                    daEn.Trades.Add(tradeData);
                }
            }

            return daEn;
        }

        private void PopulateDataList()
        {
            var dataStrList = LoadFileData();

            foreach (var item in dataStrList)
            {
                if (_tradeData.ContainsKey(item.Key) == false)
                {
                    _tradeData.Add(item.Key, GetTradeDataFile(item.Value));
                }
            }
        }


        public decimal GetLatestPrice(string symbol)
        {
            var tradeDatasFiltered = _tradeData[symbol].Trades.Where(mk => mk.DateTime == CurrentDateTime);

            //if (tradeDatasFiltered.Any())
            //{
            //    return tradeDatasFiltered.FirstOrDefault().Close;
            //}

            if (tradeDatasFiltered.FirstOrDefault() != null && tradeDatasFiltered.Any() && tradeDatasFiltered != null)
            {
                var temp = tradeDatasFiltered.FirstOrDefault();

                if (temp != null)
                {
                    return Convert.ToDecimal(temp.Close);
                }
            }
            else
            {
                tradeDatasFiltered = _tradeData[symbol].Trades.Where(mk => mk.DateTime == DataModel.RoundToNearestFiveMinDateVersion(CurrentDateTime));
                if (tradeDatasFiltered.Any())
                {
                    return Convert.ToDecimal(tradeDatasFiltered.FirstOrDefault().Close);
                }
            }

            return 0.0M;
        }

        public bool QueryOrder(string symbol)
        {
            return false;
        }

        private List<KeyValuePair<string, string>> LoadFileData()
        {
            var data = new List<KeyValuePair<string, string>>();

            string[] fileEntries = Directory.GetFiles(_configuration.GetSection("StrategyTester").GetSection("TestFiles3").Value);

            foreach (var filesItem in fileEntries)
            {
                if (File.Exists(filesItem))
                {
                    var name = filesItem.Split("_").FirstOrDefault().Split("\\").LastOrDefault();

                    // Read entire text file content in one string    
                    string text = File.ReadAllText(filesItem);
                    data.Add(new KeyValuePair<string, string>(name, text));
                }
            }

            return data;
        }

    }
}
