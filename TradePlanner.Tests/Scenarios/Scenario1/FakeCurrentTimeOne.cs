﻿using System;
using System.Collections.Generic;
using System.Text;
using TradePlanner.Interfaces;

namespace TradePlanner.Tests.Scenarios.Scenario1
{
    public class FakeCurrentTime : ICurrentDateTime
    {
        public DateTime CurrentDateTime { get; set; }

        public FakeCurrentTime()
        {
            CurrentDateTime = new DateTime();
        }

        public DateTime Time()
        {
            return CurrentDateTime;
        }
    }
}
