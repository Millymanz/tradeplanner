using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;
using TradePlanner.Tests.Scenarios.Scenario1;
using TradePlanner.Tests.Scenarios.Scenario2;
using TradePlanner.Tests.Scenarios.Scenario4;
using Xunit;

namespace TradePlanner.Tests
{
    public class TradePlannerTest
    {
        private IDataFeed _dataFeed;
        private IConfigurationRoot _configuration;
        private IDataModel _dataModel;
        public TradePlannerTest()
        {
            var environment = Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "Production";

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            _dataModel = new DataModel(_configuration);

            _dataFeed = new DataFeed(_configuration, new TimeMgr(), _dataModel);

            TradePlanner.TestMode = true;
        }

        private void RunTradePlannerTwp(StrategyTester strategyTester, FakeCurrentTime currentTimeOne, FakeDataModelOne dataModel, FakeDatafeedOne dataFeed)
        {
            currentTimeOne.CurrentDateTime = currentTimeOne.CurrentDateTime.AddMinutes(1);
            strategyTester.CurrentDateTime = currentTimeOne;
            dataFeed.CurrentDateTime = currentTimeOne.Time();
            dataModel.CurrentDateTime = currentTimeOne.Time();
            strategyTester.StartStrategyTest();

            Thread.Sleep(50);
        }

        private void RunTradePlanner(TradePlanner tradePlanner, FakeCurrentTime currentTimeOne, FakeDataModelFour dataModel, FakeDatafeedFour dataFeed)
        {
            currentTimeOne.CurrentDateTime = currentTimeOne.CurrentDateTime.AddMinutes(1);
            tradePlanner.CurrentDateTime = currentTimeOne;
            dataFeed.CurrentDateTime = currentTimeOne.Time();
            tradePlanner.CurrentDateTime = currentTimeOne;
            dataModel.CurrentDateTime = currentTimeOne.Time();
            tradePlanner.StartTradePlan();

            Thread.Sleep(60);
            //Thread.Sleep(300);
        }

        [Fact]
        public void Can_Recover_Trade_When_Target_Is_Missed()
        {
            var currentTimeOne = new FakeCurrentTime();
            currentTimeOne.CurrentDateTime = new DateTime(2021, 6, 24, 13, 0, 0);

            var dataModel = new FakeDataModelFour(_configuration, currentTimeOne.CurrentDateTime);
            var dataFeed = new FakeDatafeedFour(_configuration, currentTimeOne.CurrentDateTime);

            var timeMgr = new TimeMgr();
            var emailer = new EmailManager(_configuration);
            var reporter = new Reporter(emailer);

            var fakeTradeHandlerClient = new FakeTradeHandlerClient();

            var tradePlanner = new TradePlanner(dataModel, new EntryEngine(dataFeed, currentTimeOne, _configuration, reporter), new PatternRecognition(_configuration, dataModel),
                dataFeed, emailer, _configuration, timeMgr, fakeTradeHandlerClient, reporter, true, currentTimeOne);

            TradePlanner.TestMode = true;

            dataModel.Dataset = 4;
            dataFeed.Dataset = 4;
            dataFeed.LoadOtherDataset = true;
            fakeTradeHandlerClient.Dataset = 4;


            //ADAUSDT
            //Entry should be around 13:30 available
            //to 14:50 24th June entry
            //Sell around 1:00am 25th June

            //recovery at 25 jun 05:00

            tradePlanner.RecoveryMode.Should().BeFalse();

            ((FakeTradeHandlerClient)tradePlanner.TradeHandlerClient).GetPostNewOrderResults.Should().Be(null);

            for (int min = 1; min < 3500; min++)
            {
                RunTradePlanner(tradePlanner, currentTimeOne, dataModel, dataFeed);

                if (currentTimeOne.CurrentDateTime == new DateTime(2021, 6, 24, 14, 52, 0))
                {
                    ((FakeTradeHandlerClient)tradePlanner.TradeHandlerClient).GetPostNewOrderResults.ClientOrderId
                    .Should().Be("ADAUSDT_XXXX_BUY");
                }
            }

            tradePlanner.RecoveryMode.Should().BeTrue();
        }

        [Fact]
        public void Can_Trade_Buy_And_Sell_Based_On_Strategy()
        {
            var currentTimeOne = new FakeCurrentTime();
            currentTimeOne.CurrentDateTime = new DateTime(2021, 6, 23, 23, 0, 0);

            var dataModel = new FakeDataModelFour(_configuration, currentTimeOne.CurrentDateTime);
            var dataFeed = new FakeDatafeedFour(_configuration, currentTimeOne.CurrentDateTime); 

            var timeMgr = new TimeMgr();
            var emailer = new EmailManager(_configuration);
            var reporter = new Reporter(emailer);


            var fakeTradeHandlerClient = new FakeTradeHandlerClient();

            var tradePlanner = new TradePlanner(dataModel, new EntryEngine(dataFeed, currentTimeOne, _configuration, reporter), new PatternRecognition(_configuration, dataModel),
                dataFeed, emailer, _configuration, timeMgr, fakeTradeHandlerClient, reporter, true, currentTimeOne);

            TradePlanner.TestMode = true;

            dataModel.Dataset = 2;
            dataFeed.Dataset = 2;
            fakeTradeHandlerClient.Dataset = 2;

            //STXUSDT
            //Entry should be around 17:45 to 18:00 24th June
            //Sell around 1:00am 25th June

            ((FakeTradeHandlerClient) tradePlanner.TradeHandlerClient).GetPostNewOrderResults.Should().Be(null);

            for (int min = 1; min < 3500; min++)
            {
                RunTradePlanner(tradePlanner, currentTimeOne, dataModel, dataFeed);

                if (currentTimeOne.CurrentDateTime == new DateTime(2021, 6, 24, 17, 45, 0))
                {
                    //buy scenario
                    ((FakeTradeHandlerClient)tradePlanner.TradeHandlerClient).GetPostNewOrderResults.ClientOrderId
                    .Should().Be("STXUSDT_XXXX_BUY");

                    tradePlanner.CurrentStrategyInUse.Key.Name.Should().Be("Normal Bull Run");
                }

                if ((currentTimeOne.CurrentDateTime >= new DateTime(2021, 6, 24, 18, 30, 0))
                    &&
                    currentTimeOne.CurrentDateTime < new DateTime(2021, 6, 25, 1, 0, 0))
                {
                    //check for sell setup
                    ((FakeTradeHandlerClient)tradePlanner.TradeHandlerClient).GetAllOrdersResults.ToList().
                        Any(mm => mm.Side == "SELL" && mm.Status == "NEW" && mm.Symbol == "STXUSDT").Should().BeTrue();
                }


                if (currentTimeOne.CurrentDateTime == new DateTime(2021, 6, 25, 1, 5, 0))
                {
                    //sell scenario
                    ((FakeTradeHandlerClient)tradePlanner.TradeHandlerClient).GetAllOrdersResults.ToList().
                        Any(mm => mm.Side == "SELL" && mm.Status == "FILLED" && mm.Symbol == "STXUSDT").Should().BeTrue();
                }
            }

            tradePlanner.EntryList.Count.Should().Be(1);
            tradePlanner.RecoveryMode.Should().BeFalse();
        }

        [Fact]
        public void Can_Use_Right_Parameters()
        {
            var rees = TradePlanner.ApiAcceptableDecimalDetailed(0.08632000000M, 0.00001000M);
            rees.Should().Be(0.08632M);

            var vetusdtQty = TradePlanner.ApiAcceptableDecimalDetailed(2297.274M, 0.10000000M);
            vetusdtQty.Should().Be(2297.27M);

            var zilusdtPrice = TradePlanner.ApiAcceptableDecimalDetailed(0.08985423900000000000000M, 0.00001000M);
            zilusdtPrice.Should().Be(0.08985M);

            var shibusdtPrice = TradePlanner.ApiAcceptableDecimalDetailed(0.000008459369360003564686M, 0.00000001M);
            shibusdtPrice.Should().Be(0.00000846M);
        }

        private List<KeyValuePair<string, string>> LoadFileData()
        {
            var data = new List<KeyValuePair<string, string>>();

            string[] fileEntries = Directory.GetFiles(_configuration.GetSection("TestDataFiles").Value);

            foreach (var filesItem in fileEntries)
            {
                if (File.Exists(filesItem))
                {
                    var name = filesItem.Split("_").FirstOrDefault().Split("\\").LastOrDefault();

                    // Read entire text file content in one string    
                    string text = File.ReadAllText(filesItem);
                    data.Add(new KeyValuePair<string, string>(name, text));
                }
            }

            return data;
        }
    }
}
