﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Binance.API.Csharp.Client;
using Binance.API.Csharp.Client.Models.Account;
using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Market;
using Binance.API.Csharp.Client.Utils;
using TradePlanner.Interfaces;
using TradePlanner.Tests.Scenarios.Scenario1;

namespace TradePlanner.Tests
{
    public class FakeTradeHandlerClient : ITradeHandlerClient
    {
        public Order GetOrderResults { get; set; }
        public NewOrder GetPostNewOrderResults { get; set; }
        public CanceledOrder GetCancelOrderResults { get; set; }
        public IEnumerable<Order> GetAllOrdersResults { get; set; }
        public AccountInfo GetAccountInfoResults { get; set; }

        //must be called before any method
        public FakeCurrentTime CurrentTime { get; set; }

        //private BinanceClient _binanceClient;

        private DateTime _sellScenario1_STXUSDT;
        private DateTime _buyScenario1_STXUSDT;

        private DateTime _buyScenario1_ADAUSDT;

        public int Dataset { get; set; }

        public FakeTradeHandlerClient()
        {
            _sellScenario1_STXUSDT = new DateTime(2021, 6, 25, 1, 0, 0);
            _buyScenario1_STXUSDT =  new DateTime(2021, 6, 24, 17, 40, 0);


            _buyScenario1_ADAUSDT = new DateTime(2021, 6, 24, 14, 50, 0);
        }

        public CoinInfo GetCoinInfo(string symbol)
        {
            var coinInfo = new CoinInfo();

            return coinInfo;
        }

        public NewOrder PostNewOrderTest(string symbol, decimal origQty, decimal price, OrderSide orderSide,
            ICurrentDateTime currentDateTime)
        {
            return null;
        }

        public Order GetOrder(string symbol, int orderId, ICurrentDateTime currentDateTime)
        {
            var order = new Order();

            if (Dataset == 2)
            {
                if (currentDateTime.Time() >= _buyScenario1_STXUSDT &&
                    currentDateTime.Time() <= _buyScenario1_STXUSDT.AddMinutes(40) && symbol == "STXUSDT")
                {
                    order.Status = "FILLED";

                    GetOrderResults = order;
                }
            }

            if (Dataset == 4)
            {
                if (currentDateTime.Time() >= _buyScenario1_ADAUSDT /*&&
             currentDateTime.Time() <= _buyScenario1_ADAUSDT.AddMinutes(40)*/ && symbol == "ADAUSDT")
                {
                    order.Status = "FILLED";
                    order.OrderId = 100000;
                    order.ClientOrderId = "ADAUSDT_XXXX_BUY";

                    GetOrderResults = order;
                }
            }

            return order;
        }

        public NewOrder PostNewOrder(string symbol, decimal origQty, decimal price, OrderSide orderSide, ICurrentDateTime currentDateTime, int iteration)
        {
            var newOd = new NewOrder();

            if (Dataset == 2)
            {
                if (orderSide == OrderSide.SELL && currentDateTime.Time() >= _sellScenario1_STXUSDT &&
                    currentDateTime.Time() <= _sellScenario1_STXUSDT.AddMinutes(30) && symbol == "STXUSDT")
                {
                    //STXUSDT scenario Sell
                    newOd.OrderId = 100001;
                    newOd.ClientOrderId = "STXUSDT_XXXX_SOLD";
                    newOd.Symbol = "STXUSDT";

                    GetPostNewOrderResults = newOd;
                }
                else if (orderSide == OrderSide.BUY && currentDateTime.Time() >= _buyScenario1_STXUSDT &&
                         currentDateTime.Time() <= _buyScenario1_STXUSDT.AddMinutes(30) && symbol == "STXUSDT")
                {
                    //STXUSDT scenario Buy
                    newOd.OrderId = 100000;
                    newOd.ClientOrderId = "STXUSDT_XXXX_BUY";
                    newOd.Symbol = "STXUSDT";

                    GetPostNewOrderResults = newOd;
                }
                else if (orderSide == OrderSide.SELL && currentDateTime.Time() >= _buyScenario1_STXUSDT.AddMinutes(15))
                {
                    newOd.OrderId = 230001;
                    newOd.ClientOrderId = "STXUSDT_XXXX_SELL_LIMIT_SET";
                    newOd.Symbol = "STXUSDT";

                    GetPostNewOrderResults = newOd;
                }
            }


            if (Dataset == 4)
            {
                if (orderSide == OrderSide.BUY && currentDateTime.Time() >= _buyScenario1_ADAUSDT &&
                    currentDateTime.Time() <= _buyScenario1_ADAUSDT.AddMinutes(30) && symbol == "ADAUSDT")
                {
                    //ADAUSDT scenario Buy
                    newOd.OrderId = 100000;
                    newOd.ClientOrderId = "ADAUSDT_XXXX_BUY";
                    newOd.Symbol = "ADAUSDT";

                    GetPostNewOrderResults = newOd;
                }
                else if (orderSide == OrderSide.SELL && currentDateTime.Time() >= _buyScenario1_ADAUSDT.AddMinutes(15))
                {
                    newOd.OrderId = 842001;
                    newOd.ClientOrderId = "ADAUSDT_XXXX_SELL_LIMIT_SET";
                    newOd.Symbol = "ADAUSDT";

                    GetPostNewOrderResults = newOd;
                }
            }

            return newOd;
        }

        public CanceledOrder CancelOrder(string symbol, int orderId, ICurrentDateTime currentDateTime)
        {
            var canceledOrder = new CanceledOrder();
            canceledOrder.ClientOrderId = "order cancelled";
            canceledOrder.OrderId = orderId;
            canceledOrder.Symbol = symbol;

            GetCancelOrderResults = canceledOrder;

            return canceledOrder;
        }

        public IEnumerable<Order> GetAllOrders(string symbol, ICurrentDateTime currentDateTime)
        {
            var list = new List<Order>();

            //Collection of orders for buy sell scenario

            //if (currentDateTime.Time() >= buyScenario1 &&
            //    currentDateTime.Time() <= _sellScenario1_STXUSDT && symbol == "STXUSDT")
            //{
            //    var order = new Order();
            //    order.Side = "BUY";
            //    order.Time = Utilities.GenerateTimeStampLong(buyScenario1);

            //    list.Add(order);
            //    GetAllOrdersResults = list;
            //}

            if (Dataset == 2)
            {
                if (currentDateTime.Time() >= _buyScenario1_STXUSDT.AddMinutes(20) 
                &&
                currentDateTime.Time() < _buyScenario1_STXUSDT.AddMinutes(110))
                {
                    var order = new Order();
                    order.Side = "BUY";
                    order.Time = Utilities.GenerateTimeStampLong(_buyScenario1_STXUSDT);
                    order.Symbol = "STXUSDT";
                    order.Status = "FILLED";
                    list.Add(order);

                    var orderSell = new Order();
                    orderSell.Side = "SELL";
                    orderSell.Time = Utilities.GenerateTimeStampLong(_sellScenario1_STXUSDT);
                    orderSell.Symbol = "STXUSDT";
                    orderSell.Status = "NEW";
                    list.Add(orderSell);

                    GetAllOrdersResults = list;
                }
                else if (currentDateTime.Time() >= _sellScenario1_STXUSDT &&
                         currentDateTime.Time() < _sellScenario1_STXUSDT.AddMinutes(30) && symbol == "STXUSDT")
                {
                    var order = new Order();
                    order.Side = "BUY";
                    order.Time = Utilities.GenerateTimeStampLong(_buyScenario1_STXUSDT);
                    order.Symbol = "STXUSDT";
                    order.Status = "FILLED";
                    list.Add(order);

                    var orderSell = new Order();
                    orderSell.Side = "SELL";
                    orderSell.Time = Utilities.GenerateTimeStampLong(_sellScenario1_STXUSDT);
                    orderSell.Symbol = "STXUSDT";
                    orderSell.Status = "FILLED";
                    list.Add(orderSell);

                    GetAllOrdersResults = list;
                }
            }


            if (Dataset == 4)
            {
                if (currentDateTime.Time() >= _buyScenario1_ADAUSDT.AddMinutes(20)
                    /*&&
                    currentDateTime.Time() < _buyScenario1_ADAUSDT.AddMinutes(110)*/
                    )
                {
                    var order = new Order();
                    order.Side = "BUY";
                    order.Time = Utilities.GenerateTimeStampLong(_buyScenario1_STXUSDT);
                    order.Symbol = "ADAUSDT";
                    order.Status = "FILLED";
                    list.Add(order);

                    var orderSell = new Order();
                    orderSell.Side = "SELL";
                    orderSell.Time = Utilities.GenerateTimeStampLong(_sellScenario1_STXUSDT);
                    orderSell.Symbol = "ADAUSDT";
                    orderSell.Status = "NEW";
                    list.Add(orderSell);

                    GetAllOrdersResults = list;
                }
            }

            return list;
        }

        public IEnumerable<Order> GetCurrentOpenOrders(string symbol, ICurrentDateTime currentDateTime)
        {
            return new List<Order>();
        }

        public AccountInfo GetAccountInfo(ICurrentDateTime currentDateTime)
        {
            var balList = new List<Balance>();
            var bal = new Balance();
            bal.Free = Convert.ToDecimal(10000);
            bal.Asset = "USDT";
            balList.Add(bal);

            var accountInfo =  new AccountInfo();
            accountInfo.Balances = balList;
            accountInfo.BuyerCommission = 0;
            accountInfo.CanDeposit = true;
            accountInfo.CanTrade = true;
            accountInfo.MakerCommission = 10;
            accountInfo.SellerCommission = 0;
            accountInfo.TakerCommission = 10;

            GetAccountInfoResults = accountInfo;

            return accountInfo;
        }
    }
}
