using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using TradePlanner.Interfaces;
using TradePlanner.Tests.Scenarios.Scenario1;
using TradePlanner.Tests.Scenarios.Scenario2;
using Xunit;

namespace TradePlanner.Tests
{
    public class StrategyTesterTest
    {
        private IDataFeed _dataFeed;
        private IConfigurationRoot _configuration;
        private IDataModel _dataModel;
        public StrategyTesterTest()
        {
            var environment = Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "Production";

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            _dataModel = new DataModel(_configuration);

            _dataFeed = new DataFeed(_configuration, new TimeMgr(), _dataModel);

            TradePlanner.TestMode = true;
        }

        private void RunStrategyTester(StrategyTester strategyTester, FakeCurrentTime currentTimeOne, FakeDataModelOne dataModel, FakeDatafeedOne dataFeed)
        {
            currentTimeOne.CurrentDateTime = currentTimeOne.CurrentDateTime.AddMinutes(1);
            strategyTester.CurrentDateTime = currentTimeOne;
            dataFeed.CurrentDateTime = currentTimeOne.Time();
            dataModel.CurrentDateTime = currentTimeOne.Time();
            strategyTester.StartStrategyTest();

            Thread.Sleep(50);
        }

        private void RunStrategyTesterTwo(StrategyTester strategyTester, FakeCurrentTime currentTimeOne, FakeDataModelTwo dataModel, FakeDatafeedTwo dataFeed)
        {
            currentTimeOne.CurrentDateTime = currentTimeOne.CurrentDateTime.AddMinutes(1);
            strategyTester.CurrentDateTime = currentTimeOne;
            dataFeed.CurrentDateTime = currentTimeOne.Time();
            dataModel.CurrentDateTime = currentTimeOne.Time();
            strategyTester.StartStrategyTest();

            Thread.Sleep(35);
        }

        [Fact]
        public void Can_Validate_Strategies()
        {
            var currentTimeOne = new FakeCurrentTime();
            //time of first coin reaching rank
            currentTimeOne.CurrentDateTime = new DateTime(2021, 6, 17, 9, 0, 0);

            var dataModel = new FakeDataModelOne(_configuration, currentTimeOne.CurrentDateTime);//ranking set, //available trades set
            var dataFeed = new FakeDatafeedOne(_configuration, currentTimeOne.CurrentDateTime); //tradable coins set, //selectable by time

            var timeMgr = new TimeMgr();
            var emailer = new EmailManager(_configuration);
            var reporter = new Reporter(emailer);

            var strategyTester = new StrategyTester(dataModel, new EntryEngine(dataFeed, currentTimeOne, _configuration, reporter), new PatternRecognition(_configuration, dataModel),
                dataFeed, emailer, _configuration, timeMgr, reporter, currentTimeOne);
            strategyTester.TestMode = true;

            strategyTester.CurrentStrategyDefinition.Should().BeNull();
            strategyTester.VirtualTrades.Count.Should().Be(5);


            //9:01am
            RunStrategyTester(strategyTester, currentTimeOne, dataModel, dataFeed);
            strategyTester.EntryList.Count.Should().Be(5);


            //simulate time passing till next available trades
            for (int min = 1; min < 29; min++)
            {
                RunStrategyTester(strategyTester, currentTimeOne, dataModel, dataFeed);
            }

            strategyTester.CurrentDateTime.Time().Should().Be(new DateTime(2021, 6, 17, 9, 29, 0));

            RunStrategyTester(strategyTester, currentTimeOne, dataModel, dataFeed);


            strategyTester.CurrentDateTime.Time().Should().Be(new DateTime(2021, 6, 17, 9, 30, 0));
            strategyTester.EntryList.Count.Should().Be(10);

            //we exceed and iterate to and beyond the entrytime
            //we expect the virtual trades for these to have began although not all
            for (int min = 1; min <= 400; min++) 
            {
                RunStrategyTester(strategyTester, currentTimeOne, dataModel, dataFeed);
            }

            //strategyTester.CurrentStrategyDefinition.Should().BeNull();

            strategyTester.CurrentDateTime.Time().Should().Be(new DateTime(2021, 6, 17, 16, 10, 0));

            //given all have made an entry lets see if we make profit or sell virtually in the next 10 hours max
            for (int min = 1; min <= 1080; min++)
            {
                RunStrategyTester(strategyTester, currentTimeOne, dataModel, dataFeed);
            }

            strategyTester.CurrentDateTime.Time().Should().Be(new DateTime(2021, 6, 18, 10, 10, 0));

            //these unit/integration test are muliti-threaded and therefore, some threads overlap or one can finish before the other
            //therefore in this context only the strategy can be one or the other, whichever one finishes first.
            //strategyTester.CurrentStrategyDefinition.Name.Should().BeOneOf("Normal Bull Run", "Excellent Bull Run");

            strategyTester.CurrentStrategyDefinition.Name.Should().Be("Excellent Bull Run");
        }

        [Fact]
        public void Can_Switch_Strategies()
        {
            //demo how u can go from 10% strat to 5% to 10%

            var currentTimeOne = new FakeCurrentTime();
            //time of first coin reaching rank
            currentTimeOne.CurrentDateTime = new DateTime(2021, 5, 5, 0, 5, 0);

            var dataModel = new FakeDataModelTwo(_configuration, currentTimeOne.CurrentDateTime);//ranking set, //available trades set
            var dataFeed = new FakeDatafeedTwo(_configuration, currentTimeOne.CurrentDateTime); //tradable coins set, //selectable by time

            var timeMgr = new TimeMgr();
            var emailer = new EmailManager(_configuration);
            var reporter = new Reporter(emailer);

            var strategyTester = new StrategyTester(dataModel, new EntryEngine(dataFeed, currentTimeOne, _configuration, reporter), new PatternRecognition(_configuration, dataModel),
                dataFeed, emailer, _configuration, timeMgr, reporter, currentTimeOne);
            strategyTester.TestMode = true;

            for (int min = 1; min <= 1581; min++) //1280
            {
                RunStrategyTesterTwo(strategyTester, currentTimeOne, dataModel, dataFeed);
            }

            strategyTester.CurrentStrategyDefinition.Name.Should().Be("Excellent Bull Run");
            strategyTester.VirtualTrades.Count.Should().Be(5);

            //get data for another period and see if it will switch to 5% strategy
            //iterate time forward

            currentTimeOne.CurrentDateTime = new DateTime(2021, 5, 10, 0, 5, 0);
            dataFeed.Dataset = 2; //select the other dataset for  the new day
            dataModel.Dataset = 2;
            dataFeed.Clear();
            dataFeed.PopulateDataList();

            for (int min = 1; min <= 5001; min++)
            {
                RunStrategyTesterTwo(strategyTester, currentTimeOne, dataModel, dataFeed);
            }

            strategyTester.CurrentStrategyDefinition.Name.Should().Be("Normal Bull Run");


            currentTimeOne.CurrentDateTime = new DateTime(2021, 6, 17, 0, 10, 0);
            dataFeed.Dataset = 3; //select the other dataset for  the new day
            dataModel.Dataset = 3;
            dataFeed.Clear();
            dataFeed.PopulateDataList();

            var ten = new DateTime(2021, 6, 18, 10, 10, 0);

            var expireDateTimeResetIndicator = new DateTime(2050, 1, 1);

            for (int min = 1; min <= 4601; min++) //excessive run, beyond the test period
            {
                RunStrategyTesterTwo(strategyTester, currentTimeOne, dataModel, dataFeed);

                if (ten == strategyTester.CurrentDateTime.Time())
                {
                    strategyTester.CurrentStrategyDefinition.Name.Should().Be("Excellent Bull Run");
                }

                if (expireDateTimeResetIndicator == strategyTester.CurrentDateTime.Time())
                {
                    strategyTester.VirtualTrades.FirstOrDefault().Value.Count.Should().Be(0);
                }
            }
            
            //strategy switch
            //as the coins are continously tested and new entries are generated following the expiry of the last run
            //
            strategyTester.CurrentStrategyDefinition.Name.Should().Be("Excellent Bull Run");
        }

        private List<KeyValuePair<string, string>> LoadFileData()
        {
            var data = new List<KeyValuePair<string, string>>();

            string[] fileEntries = Directory.GetFiles(_configuration.GetSection("TestDataFiles").Value);

            foreach (var filesItem in fileEntries)
            {
                if (File.Exists(filesItem))
                {
                    var name = filesItem.Split("_").FirstOrDefault().Split("\\").LastOrDefault();

                    // Read entire text file content in one string    
                    string text = File.ReadAllText(filesItem);
                    data.Add(new KeyValuePair<string, string>(name, text));
                }
            }

            return data;
        }
    }
}
