﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Binance.API.Csharp.Client.Models.Enums;
using System.Threading;
using Binance.API.Csharp.Client.Models.WebSocket;

namespace Binance.API.Csharp.Client.Test
{
    [TestClass]
    public class BinanceTest
    {
        //private static ApiClient apiClient = new ApiClient("Dz3NkqL8ZVuof2IKmPxTCbCDCdrSgIDM1hY0fC35jTVyUQuxfIKYVaKSs0ZK7KS6",
        //    "yKb69Lf1ZfzRNEPvoZPihGO047i7I7DPCjRDuTNQQVIkZHnBaoJMSSgGz5smCqRQ");

        private static ApiClient apiClient = new ApiClient("",
            "");


        private static BinanceClient binanceClient = new BinanceClient(apiClient,false);

        #region General
        [TestMethod]
        public void TestConnectivity()
        {
            var test = binanceClient.TestConnectivity().Result;
        }

        [TestMethod]
        public void GetServerTime()
        {
            var serverTime = binanceClient.GetServerTime().Result;
        }
        #endregion

        #region Market Data
        [TestMethod]
        [Ignore]

        public void GetOrderBook()
        {
            var orderBook = binanceClient.GetOrderBook("ethbtc").Result;
        }

        [TestMethod]
        [Ignore]

        public void GetCandleSticks()
        {
            var candlestick = binanceClient.GetCandleSticks("ethbtc", TimeInterval.Minutes_15, new System.DateTime(2017,11,24), new System.DateTime(2017, 11, 26)).Result;
        }

        [TestMethod]
        [Ignore]

        public void GetAggregateTrades()
        {
            var aggregateTrades = binanceClient.GetAggregateTrades("ethbtc").Result;
        }

        [TestMethod]
        [Ignore]

        public void GetPriceChange24H()
        {
            var singleTickerInfo = binanceClient.GetPriceChange24H("ETHBTC").Result;

            var allTickersInfo = binanceClient.GetPriceChange24H().Result;
        }

        [TestMethod]
        [Ignore]

        public void GetAllPrices()
        {
            var tickerPrices = binanceClient.GetAllPrices().Result;
        }

        [TestMethod]
        [Ignore]

        public void GetOrderBookTicker()
        {
            var orderBookTickers = binanceClient.GetOrderBookTicker().Result;
        }
        #endregion

        #region Account Information
        [TestMethod]
        [Ignore]
        public void PostLimitOrder()
        {
            var buyOrder = binanceClient.PostNewOrder("SHIBUSDT", 10964912m, 0.00000875m, OrderSide.BUY).Result;

            //var sellOrder = binanceClient.PostNewOrder("LUNAUSDT", 1.8m, 6.386M, OrderSide.SELL).Result;
        }

        [TestMethod]
        [Ignore]

        public void PostMarketOrder()
        {
            var buyMarketOrder = binanceClient.PostNewOrder("ethbtc", 0.01m, 0m, OrderSide.BUY, OrderType.MARKET).Result;
            var sellMarketOrder = binanceClient.PostNewOrder("ethbtc", 0.01m, 0m, OrderSide.SELL, OrderType.MARKET).Result;
        }

        [TestMethod]
        [Ignore]

        public void PostIcebergOrder()
        {
            var icebergOrder = binanceClient.PostNewOrder("ethbtc", 0.01m, 0m, OrderSide.BUY, OrderType.MARKET, icebergQty: 2m).Result;
        }

        [TestMethod]
        [Ignore]

        public void PostNewLimitOrderTest()
        {
            var testOrder = binanceClient.PostNewOrderTest("ethbtc", 1m, 0.1m, OrderSide.BUY).Result;
        }

        [TestMethod]
        [Ignore]

        public void CancelOrder()
        {
            var canceledOrder = binanceClient.CancelOrder("ethbtc", 9137796).Result;
        }

        [TestMethod]
        [Ignore]
        public void GetCurrentOpenOrders()
        {
            var openOrders = binanceClient.GetCurrentOpenOrders("ethbtc").Result;
        }

        [TestMethod]
        [Ignore]

        public void GetOrder()
        {
            var order = binanceClient.GetOrder("DOTUSDT", 1355420297).Result;
        }

        [TestMethod]
        [Ignore]

        public void GetAllOrders()
        {
            var allOrders = binanceClient.GetAllOrders("ZILUSDT").Result;
            
            var temp = allOrders.Where(uo => uo.Side == "SELL" && uo.Status == "NEW" && uo.Symbol == "njk")
                .FirstOrDefault();
        }

        [TestMethod]
        [Ignore]

        public void GetAccountInfo()
        {
            var accountInfo = binanceClient.GetAccountInfo().Result;
        }

        [TestMethod]
        [Ignore]
        public void GetTradeList()
        {
            var tradeList = binanceClient.GetTradeList("ethbtc").Result;
        }

        [TestMethod]
        [Ignore]

        public void Withdraw()
        {
            var withdrawResult = binanceClient.Withdraw("AST", 100m, "@YourDepositAddress").Result;
        }

        [TestMethod]
        [Ignore]

        public void GetDepositHistory()
        {
            var depositHistory = binanceClient.GetDepositHistory("neo", DepositStatus.Success).Result;
        }

        [TestMethod]
        [Ignore]

        public void GetWithdrawHistory()
        {
            var withdrawHistory = binanceClient.GetWithdrawHistory("neo").Result;
        }
        #endregion

        #region User stream
        [TestMethod]
        [Ignore]

        public void StartUserStream()
        {
            var listenKey = binanceClient.StartUserStream().Result.ListenKey;
        }

        [TestMethod]
        [Ignore]

        public void KeepAliveUserStream()
        {
            var ping = binanceClient.KeepAliveUserStream("@ListenKey").Result;
        }

        [TestMethod]
        [Ignore]

        public void CloseUserStream()
        {
            var resut = binanceClient.CloseUserStream("@ListenKey").Result;
        }
        #endregion

        #region WebSocket

        #region Depth
        private void DepthHandler(DepthMessage messageData)
        {
            var depthData = messageData;
        }

        [TestMethod]
        [Ignore]

        public void TestDepthEndpoint()
        {
            binanceClient.ListenDepthEndpoint("ethbtc", DepthHandler);
            Thread.Sleep(50000);
        }

        #endregion

        #region Kline
        private void KlineHandler(KlineMessage messageData)
        {
            var klineData = messageData;
        }

        [TestMethod]
        [Ignore]

        public void TestKlineEndpoint()
        {
            binanceClient.ListenKlineEndpoint("ethbtc", TimeInterval.Minutes_1, KlineHandler);
            Thread.Sleep(50000);
        }
        #endregion

        #region AggregateTrade
        private void AggregateTradesHandler(AggregateTradeMessage messageData)
        {
            var aggregateTrades = messageData;
        }

        [TestMethod]
        [Ignore]
        public void AggregateTestTradesEndpoint()
        {
            binanceClient.ListenTradeEndpoint("ethbtc", AggregateTradesHandler);
            Thread.Sleep(50000);
        }

        #endregion

        #region User Info
        private void AccountHandler(AccountUpdatedMessage messageData)
        {
            var accountData = messageData;
        }

        private void TradesHandler(OrderOrTradeUpdatedMessage messageData)
        {
            var tradesData = messageData;
        }

        private void OrdersHandler(OrderOrTradeUpdatedMessage messageData)
        {
            var ordersData = messageData;
        }

        [TestMethod]
        [Ignore]

        public void TestUserDataEndpoint()
        {
            binanceClient.ListenUserDataEndpoint(AccountHandler, TradesHandler, OrdersHandler);
            Thread.Sleep(50000);
        }
        #endregion

        #endregion
    }
}
